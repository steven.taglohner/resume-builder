import { DeleteOutlined, EditOutlined, PrinterOutlined } from '@ant-design/icons';
import { MouseEvent, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useIntl } from 'react-intl';
import { withAuth } from 'src/HOCs';
import { Button, Card, Content, PageHeader, PopConfirm, Space } from 'src/components';
import { createResume, deleteResume, getResumeList } from 'src/redux/actions';
import { PrivateLayout } from 'src/layouts';
import { ROUTE } from 'src/types';
import { useDispatch, useSelector } from 'src/hooks';
import styles from './dashboard.module.css';

const Dashboard = () => {
	const { formatMessage } = useIntl();
	const dispatch = useDispatch();
	const router = useRouter();

	const data = useSelector(state => state.resume.list);

	useEffect(() => {
		dispatch(
			getResumeList()
		);
	}, [dispatch]);

	const handleEdit = (event: MouseEvent<HTMLDivElement>) => {
		router.push(ROUTE.RESUME + event.currentTarget.id);
	};

	const handleNewResume = () => {
		dispatch(
			createResume()
		);
	};

	const handleDelete = (id: string) => {
		dispatch(
			deleteResume(id)
		);
	};

	const handlePrint = (event: MouseEvent<HTMLDivElement>) => {
		console.log('PRINTED');
	};

	return (
		<PrivateLayout>
			<Content>
				<PageHeader
					title={formatMessage({ id: 'resumes' })}
					extra={[
						<Button
							key="create-new"
							type="primary"
							onClick={handleNewResume}
						>
							{formatMessage({ id: 'new-resume' })}
						</Button>
					]}
				/>
				<Space wrap>
					{data.map(resume => (
						<Card
							hoverable
							key={resume.id}
							id={resume.id}
							className={styles.card}
							actions={[
								<PrinterOutlined
									key={resume.id + 'print'}
									onClick={handlePrint}
								/>,
								<EditOutlined
									id={resume.id}
									key={resume.id + 'print'}
									onClick={handleEdit}
								/>,
								<PopConfirm
									id={resume.id}
									arrowPointAtCenter
									key={resume.id + 'delete'}
									onConfirm={() => handleDelete(resume.id)}
									title={formatMessage({ id: 'delete-resume-confirmation' })}
									okText="Yes"
									cancelText="No"
									placement="bottom"
								>
									<DeleteOutlined />
								</PopConfirm>
							]}
						>
							<Card.Meta
								title={resume.title}
								description={resume.id}
							/>
						</Card>
					))}
				</Space>
			</Content>
		</PrivateLayout>
	);
};

export default withAuth(Dashboard);