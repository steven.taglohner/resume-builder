import { useEffect, Fragment } from 'react';
import { useRouter } from 'next/router';
import { withAuth } from 'src/HOCs';
import { useDispatch } from 'src/hooks';
import { getResume } from 'src/redux/actions';
import { Content, Space } from 'src/components';
import {
    Preview,
    PersonalInformation,
    ProfessionalSummary,
    EmploymentHistory,
    Education,
    Skills,
    Hobbies,
    Courses,
    Languages,
    Internships,
    References,
    VolunteerWork
} from './sections';
import styles from './resume.module.css';

const Resume = () => {
    const dispatch = useDispatch();
    const router = useRouter();

    useEffect(() => {
        if (typeof router.query.id === 'string') {
            dispatch(
                getResume(router.query.id)
            );
        }
    }, [dispatch, router]);

    return (
        <Fragment>
            <Content className={styles.resume}>
                {<Space className={styles.space} direction="vertical" size={[24, 24]}>
                    <PersonalInformation />
                    <ProfessionalSummary />
                    <EmploymentHistory />
                    <Education />
                    <Internships />
                    <Skills />
                    <Courses />
                    <Languages />
                    <References />
                    <VolunteerWork />
                    <Hobbies />
                </Space>}
            </Content>
            <Preview />
        </Fragment>
    );
};

export default withAuth(Resume);
