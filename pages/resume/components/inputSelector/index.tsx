import { useId } from 'react';
import { useIntl } from 'react-intl';
import { Col, Form, Row } from 'src/components';
import { InputItem } from 'src/types';

type InputSelectorProps = {
    model: InputItem[],
}

export const InputSelector = (props: InputSelectorProps) => {
    const { formatMessage } = useIntl();
    const itemId = useId();

    return (
        <Row gutter={[24, 0]}>
            {props.model.map(
                ({ label, message, required, component: Component, columnProps, ...rest }, index) => (
                    <Col key={`${itemId}-${index}`} xs={{ span: 24 }} lg={{ span: 12 }} {...columnProps} >
                        <Form.Item
                            label={formatMessage({ id: label })}
                            rules={[{ required, message: formatMessage({ id: message }) }]}
                            {...rest}
                        >
                            <Component />
                        </Form.Item>
                    </Col>
                ))
            }
        </Row >
    );
};