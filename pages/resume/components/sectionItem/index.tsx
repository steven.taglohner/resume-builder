import { Card, Space, Text } from 'src/components';
import { SECTION, SectionItem as SectionItemType } from 'src/types';
import classnames from 'classnames';
import classes from './section-item.module.css';

type SectionItemProps = {
    name: string,
    section: SECTION,
    disabled: boolean,
    onClick: (section: SECTION) => void,
    icon: any
}

export const SectionItem = ({ name, section, disabled, onClick, icon: Icon }: SectionItemProps) => {

    const handleClick = (section: SECTION) => {
        if (onClick)
            onClick(section)
    }

    return (
        <Card
            bodyStyle={{ padding: 0, paddingTop: 10, }}
            bordered={false}
            onClick={() => handleClick(section)}
            className={classnames(classes.card, { [classes.disabled]: disabled })}
        >
            <Space size={4} className={classes.space}>
                <Icon className={classnames(classes.icon, { [classes.disabled]: disabled })} />
                <Text className={classnames(classes.title, { [classes.disabled]: disabled })}>{name}</Text>
            </Space>
        </Card>
    );
}