import { ReactNode } from 'react';
import { Content } from 'src/components';
import { Footer } from './footer';
import { Header } from './header';
import { Skeleton } from './skeleton';
import { Fallback } from './fallback';
import classes from './section.module.css';

type SectionProps = {
    children: ReactNode | ReactNode[]
}

export const Section = ({ children }: SectionProps) => (
    <Content className={classes.content}>
        {children}
    </Content>
);

Section.Footer = Footer;
Section.Header = Header;
Section.Skeleton = Skeleton;
Section.Fallback = Fallback
