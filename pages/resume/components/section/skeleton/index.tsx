import { ReactNode, useId } from 'react';
import { DropResult, ResponderProvided } from 'react-beautiful-dnd';
import { DeleteOutlined } from '@ant-design/icons';
import { useIntl } from 'react-intl';
import { Draggable, Droppable, Content, Collapse, Panel, Button, Text } from 'src/components';
import classes from './skeleton.module.css';

type SkeletonProps<T> = {
    data: T[];
    children: (item: T) => ReactNode | ReactNode[],
    onDragEnd: (result: DropResult, provided: ResponderProvided) => void;
    onDelete: (id: string) => void;
    activeKey?: string | number
    header: {
        getTitle: (item: T) => string | undefined,
        getSubtitle: (item: T) => string | undefined,
    }
    fallback?: ReactNode | ReactNode[]
}

export const Skeleton = <T extends { id: string }>({
    data,
    children,
    onDragEnd,
    onDelete,
    header: {
        getTitle,
        getSubtitle
    },
    fallback
}: SkeletonProps<T>) => {
    const dragabbleId = useId();
    const { formatMessage } = useIntl();

    const renderHeader = (item: T) => {
        const title = getTitle(item);
        const subtitle = getSubtitle(item);

        return (
            <Content className={classes.header}>
                {title
                    ? <Text className={classes.title} strong>{title}</Text>
                    : <Text type='danger' strong>
                        {formatMessage({ id: 'not-specified' })}
                    </Text>}
                {title && subtitle && <Text>{subtitle}</Text>}
            </Content>
        );
    };

    const renderFooter = ({ id }: T) => (
        <Button danger type="link" size="small"
            onClick={() => onDelete(id)}
            icon={<DeleteOutlined />}
        >
            {formatMessage({ id: 'delete' })}
        </Button>
    );

    return (
        <Content className={classes.content}>
            <Droppable onDragEnd={onDragEnd}>
                {data.length > 0
                    ? data.map((item, index) => <Draggable
                        draggableId={`${dragabbleId}-${index}`}
                        index={index}
                        key={`${dragabbleId}-${index}`}
                    >
                        <Collapse key={item.id}>
                            <Panel
                                className={classes.panel}
                                key={item.id}
                                header={renderHeader(item)}
                                footer={renderFooter(item)}
                            >
                                {children(item)}
                            </Panel>
                        </Collapse>
                    </Draggable>)
                    : fallback}
            </Droppable>
        </Content>
    );
};

Skeleton.defaultProps = {
    fallback: null
}
