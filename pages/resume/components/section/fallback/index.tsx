import { MouseEvent } from 'react';
import { PlusCircleOutlined } from '@ant-design/icons';
import { Button } from 'src/components';
import classes from './fallback.module.css'

type FallbackProps = {
    title: string,
    onClick: (event: MouseEvent<HTMLElement>) => void
}

export const Fallback = ({ title, onClick }: FallbackProps) => (
    <Button
        block
        icon={<PlusCircleOutlined />}
        type="dashed"
        className={classes.button}
        onClick={onClick}
    >
        {title}
    </Button>
);
