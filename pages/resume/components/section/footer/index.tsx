import { MouseEvent } from 'react';
import { useIntl } from 'react-intl';
import { PlusCircleFilled } from '@ant-design/icons';
import { Button } from 'src/components';
import styles from './footer.module.css';

type SectionFooterProps = {
    buttonTitleId: string
    onCreate: (event: MouseEvent<HTMLElement>) => void
}

export const Footer = ({ onCreate, buttonTitleId }: SectionFooterProps) => {
    const { formatMessage } = useIntl();

    return (
        <Button
            className={styles.button}
            type="link"
            icon={<PlusCircleFilled />}
            onClick={onCreate}
        >
            {formatMessage({ id: buttonTitleId })}
        </Button>
    );
};