import { useIntl } from 'react-intl';
import { Title, Paragraph } from 'src/components';

type SectionHeaderProps = {
    titleId: string,
    descriptionId?: string
}

export const SectionHeader = ({ titleId, descriptionId }: SectionHeaderProps) => {
    const { formatMessage } = useIntl();

    return (
        <>
            <Title level={5}>
                {formatMessage({ id: titleId })}
            </Title>
            {descriptionId && <Paragraph>
                {formatMessage({ id: descriptionId })}
            </Paragraph>}
        </>
    );
};