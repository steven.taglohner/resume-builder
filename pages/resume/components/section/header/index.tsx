import { useIntl } from 'react-intl';
import { Title, Paragraph } from 'src/components';
import styles from './header.module.css'

type HeaderProps = {
    titleId: string,
    descriptionId?: string
}

export const Header = ({ titleId, descriptionId }: HeaderProps) => {
    const { formatMessage } = useIntl();

    return (
        <>
            <Title level={5} className={styles.title}>
                {formatMessage({ id: titleId })}
            </Title>
            {descriptionId && <Paragraph className={styles.description}>
                {formatMessage({ id: descriptionId })}
            </Paragraph>}
        </>
    );
};