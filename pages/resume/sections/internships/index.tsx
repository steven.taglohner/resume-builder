import { memo } from 'react';
import { useIntl } from 'react-intl';
import { DropResult } from 'react-beautiful-dnd';
import { Form } from 'src/components';
import { useSelector, useDispatch } from 'src/hooks/redux';
import { updateResume } from 'src/redux/actions';
import { model } from './model';
import { SECTION } from 'src/types';
import { Internship } from 'src/types/resume';
import { Section, InputSelector } from 'pages/resume/components';
import { getSubtitle, getTitle, reorder } from 'pages/resume/helpers';
import { generateKey } from 'src/utils';


const Internship = () => {
    const { formatMessage } = useIntl();
    const dispatch = useDispatch();

    const resume = useSelector(state => state.resume.data);

    const handleUpdate = (values: Partial<Internship>, id: string): void => {
        dispatch(
            updateResume({
                updates: resume.internships.map(item =>
                    item.id === id ? { ...item, ...values } : item
                ),
                key: SECTION.INTERNSHIPS,
                id: resume.id,
            })
        );
    };

    const handleDragEnd = (result: DropResult) => {
        if (!result.destination) return;

        dispatch(
            updateResume({
                key: SECTION.INTERNSHIPS,
                id: resume.id,
                updates: reorder<Internship>(
                    resume.internships,
                    result.source.index,
                    result.destination.index
                )
            })
        );
    };

    const handleCreate = () => {
        dispatch(
            updateResume({
                updates: [...resume.internships, { id: generateKey() }],
                key: SECTION.INTERNSHIPS,
                id: resume.id,
                debounce: false,
            })
        );
    };

    const handleDelete = (id: string): void => {
        dispatch(
            updateResume({
                updates: resume.internships.filter(employment => employment.id !== id),
                key: SECTION.INTERNSHIPS,
                id: resume.id,
            })
        );
    };

    if (!resume)
        return null

    return (
        <Section>
            <Section.Header titleId="internships" />
            <Section.Skeleton
                data={resume.internships}
                onDragEnd={handleDragEnd}
                onDelete={handleDelete}
                fallback={<Section.Fallback
                    title={formatMessage({ id: 'add-internship' })}
                    onClick={handleCreate}
                />}
                header={{
                    getTitle: ({ employer, title }) => getTitle({
                        title: title,
                        location: employer,
                        preposition: formatMessage({ id: 'at' })
                    }),
                    getSubtitle: ({ range }) => getSubtitle({
                        presentText: formatMessage({ id: 'present' }),
                        range,
                    })
                }}
            >
                {(item: Internship) => (
                    <Form
                        layout="vertical"
                        initialValues={item}
                        onValuesChange={values => handleUpdate(values, item.id)}
                    >
                        <InputSelector model={model} />
                    </Form>
                )}
            </Section.Skeleton>
            {resume.internships.length > 0 && <Section.Footer
                buttonTitleId='add-internship'
                onCreate={handleCreate}
            />}
        </Section>
    )
};

export default memo(Internship);