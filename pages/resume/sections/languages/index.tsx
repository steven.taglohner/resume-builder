import { memo } from 'react';
import { useIntl } from 'react-intl';
import { DropResult } from 'react-beautiful-dnd';
import { Form } from 'src/components';
import { useSelector, useDispatch } from 'src/hooks/redux';
import { updateResume } from 'src/redux/actions';
import { model } from './model';
import { SECTION } from 'src/types';
import { Language } from 'src/types/resume';
import { Section, InputSelector } from 'pages/resume/components';
import { reorder } from 'pages/resume/helpers';
import { generateKey } from 'src/utils';
import { lowerCase } from 'lodash';

const Languages = () => {
    const { formatMessage } = useIntl();
    const dispatch = useDispatch();
    const resume = useSelector(state => state.resume.data);

    const handleUpdate = (values: Partial<Language>, id: string): void => {
        dispatch(
            updateResume({
                updates: {
                    ...resume.languages,
                    data: resume.languages.data.map(item =>
                        item.id === id ? { ...item, ...values } : item
                    ),
                },
                key: SECTION.LANGUAGES,
                id: resume.id,
            })
        );
    };

    const handleDragEnd = (result: DropResult) => {
        if (!result.destination) return;

        dispatch(
            updateResume({
                key: SECTION.LANGUAGES,
                id: resume.id,
                updates: {
                    ...resume.languages,
                    data: reorder<Language>(
                        resume.languages.data,
                        result.source.index,
                        result.destination.index
                    )
                },
            })
        );
    };

    const handleCreate = () => {
        dispatch(
            updateResume({
                updates: {
                    ...resume.languages,
                    data: [...resume.languages.data, {
                        id: generateKey(),
                        level: 'Beginner'
                    }]
                },
                key: SECTION.LANGUAGES,
                id: resume.id,
                debounce: false
            })
        );
    };

    const handleDelete = (id: string): void => {
        dispatch(
            updateResume({
                updates: {
                    ...resume.languages,
                    data: resume.languages.data.filter(language => language.id !== id)
                },
                key: SECTION.LANGUAGES,
                id: resume.id,
            })
        );
    };

    if (!resume)
        return null

    return (
        <Section>
            <Section.Header
                titleId="languages"
            />
            <Section.Skeleton
                data={resume.languages.data}
                onDragEnd={handleDragEnd}
                onDelete={handleDelete}
                fallback={
                    <Section.Fallback
                        title={formatMessage({ id: 'add-language' })}
                        onClick={handleCreate}
                    />}
                header={{
                    getTitle: ({ language }) => language,
                    getSubtitle: ({ level }) => level
                        ? formatMessage({ id: lowerCase(level) })
                        : ''
                }}
            >
                {(item: Language) => (
                    <Form
                        layout="vertical"
                        initialValues={item}
                        onValuesChange={values => handleUpdate(values, item.id)}
                    >
                        <InputSelector model={model} />
                    </Form>
                )}
            </Section.Skeleton>
            {resume.languages.data.length > 0 && <Section.Footer
                buttonTitleId='add-language'
                onCreate={handleCreate}
            />}
        </Section>
    )
};

export default memo(Languages);