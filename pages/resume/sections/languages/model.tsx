import { InputItem } from 'src/types';
import { Input, LevelSelector } from 'src/components';

export const model: InputItem[] = [{
    component: Input,
    label: 'language',
    required: false,
    message: 'recommended-info',
    name: 'language',
},
{
    component: LevelSelector,
    label: 'level',
    required: false,
    message: 'recommended-info',
    name: 'level',
}];
