import { InputItem } from 'src/types';
import { Input, LevelSelector } from 'src/components';

export const model: InputItem[] = [{
    component: Input,
    label: 'skill',
    required: false,
    message: 'recommended-info',
    name: 'skill'
},
{
    component: LevelSelector,
    label: 'level',
    required: false,
    message: 'recommended-info',
    name: 'level'
}];
