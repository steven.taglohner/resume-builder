import { memo } from 'react';
import { useIntl } from 'react-intl';
import { DropResult } from 'react-beautiful-dnd';
import { Form } from 'src/components';
import { useSelector, useDispatch } from 'src/hooks/redux';
import { updateResume } from 'src/redux/actions';
import { model } from './model';
import { SECTION } from 'src/types';
import { Skill } from 'src/types/resume';
import { Section, InputSelector } from 'pages/resume/components';
import { reorder } from 'pages/resume/helpers';
import { generateKey } from 'src/utils';
import { lowerCase } from 'lodash';

const Skills = () => {
    const { formatMessage } = useIntl();
    const dispatch = useDispatch();
    const resume = useSelector(state => state.resume.data);

    const handleUpdate = (values: Partial<Skill>, id: string): void => {
        dispatch(
            updateResume({
                updates: {
                    ...resume.skills,
                    data: resume.skills.data.map(item =>
                        item.id === id ? { ...item, ...values } : item
                    ),
                },
                key: SECTION.SKILLS,
                id: resume.id,
            })
        );
    };

    const handleDragEnd = (result: DropResult) => {
        if (!result.destination) return;

        dispatch(
            updateResume({
                key: SECTION.SKILLS,
                id: resume.id,
                updates: {
                    ...resume.skills,
                    data: reorder<Skill>(
                        resume.skills.data,
                        result.source.index,
                        result.destination.index
                    )
                },
            })
        );
    };

    const handleCreate = () => {
        dispatch(
            updateResume({
                updates: {
                    ...resume.skills,
                    data: [...resume.skills.data, {
                        id: generateKey(),
                        level: 'Beginner'
                    }]
                },
                key: SECTION.SKILLS,
                id: resume.id,
                debounce: false
            })
        );
    };

    const handleDelete = (id: string): void => {
        dispatch(
            updateResume({
                updates: {
                    ...resume.skills,
                    data: resume.skills.data.filter(skill => skill.id !== id)
                },
                key: SECTION.SKILLS,
                id: resume.id,
            })
        );
    };

    if (!resume)
        return null

    return (
        <Section>
            <Section.Header
                titleId="skills"
                descriptionId="skills-description"
            />
            <Section.Skeleton
                data={resume.skills.data}
                onDragEnd={handleDragEnd}
                onDelete={handleDelete}
                fallback={
                    <Section.Fallback
                        title={formatMessage({ id: 'add-skill' })}
                        onClick={handleCreate}
                    />}
                header={{
                    getTitle: ({ skill }) => skill,
                    getSubtitle: ({ level }) => level
                        ? formatMessage({ id: lowerCase(level) })
                        : ''
                }}
            >
                {(item: Skill) => (
                    <Form
                        layout="vertical"
                        initialValues={item}
                        onValuesChange={values => handleUpdate(values, item.id)}
                    >
                        <InputSelector model={model} />
                    </Form>
                )}
            </Section.Skeleton>
            {resume.skills.data.length > 0 && <Section.Footer
                buttonTitleId='add-skill'
                onCreate={handleCreate}
            />}
        </Section>
    )
};

export default memo(Skills);