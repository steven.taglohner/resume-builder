import { memo } from "react"
import { useIntl } from 'react-intl';
import { Col, Row } from 'src/components';
import { Section, SectionItem } from 'pages/resume/components';
import classes from './styles.module.css'
import { Resume, SECTION, SectionItem as SectionItemType } from "src/types";
import { useSelector, useDispatch } from "src/hooks";
import { updateResume } from 'src/redux/actions';
import { generateKey } from 'src/utils';
import { model } from './model'

const sectionManager = () => {
    const { formatMessage } = useIntl();
    const dispatch = useDispatch();
    const resume = useSelector(state => state.resume.data)

    const handleAddSection = (section: SECTION): void => {
        dispatch(
            updateResume({
                updates: resume[section] instanceof Array ? [{ id: generateKey() }] : { description: "" },
                key: section,
                id: resume.id,
                debounce: false
            })
        );
    }

    const handleDisableSection = (resume: Resume, section: SECTION) => {
        console.log({ resume, section })
        if (resume[section] === undefined)
            return false

        if (typeof resume[section] === 'object') {
            return Object.keys(resume[section]).length > 0
        }

        if (resume[section] instanceof Array) {
            // @ts-ignore 
            return resume[key].length > 0
        }
    }

    // TODO: decide later how to handle empty data
    if (!resume)
        return null

    return (
        <>
            <Section.Header titleId='add-section' />
            <Row gutter={[16, 10]}>
                {model.map(item => (
                    <Col className={classes.item}
                        key={`${item.section}-${item.name}`}
                        xs={{ span: 24 }}
                        md={{ span: 12 }}
                        lg={{ span: 8 }}
                    >
                        <SectionItem
                            {...item}
                            disabled={handleDisableSection(resume, item.section)}
                            onClick={handleAddSection}
                            name={formatMessage({ id: item.name })} />
                    </Col>
                ))}
            </Row>
        </>
    )
}

sectionManager.defaultProps = {
    items: [],
}

export default memo(sectionManager)
