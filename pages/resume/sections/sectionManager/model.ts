import { EyeOutlined, TranslationOutlined, GroupOutlined } from '@ant-design/icons';
import { SECTION, SectionItem } from 'src/types';

export const model: SectionItem[] = [
    {
        name: 'courses',
        section: SECTION.COURSES,
        icon: EyeOutlined
    },
    {
        name: 'volunteer-work',
        section: SECTION.VOLUNTEER_WORK,
        icon: EyeOutlined
    },
    {
        name: 'languages',
        section: SECTION.LANGUAGES,
        icon: TranslationOutlined
    },
    {
        name: 'references',
        section: SECTION.REFERENCES,
        icon: GroupOutlined
    },
    {
        name: 'internships',
        section: SECTION.INTERNSHIPS,
        icon: EyeOutlined
    },
    {
        name: 'hobbies',
        section: SECTION.HOBBIES,
        icon: EyeOutlined
    },
]