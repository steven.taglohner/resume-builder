import { InputItem } from 'src/types';
import { Input } from 'src/components';

export const model: InputItem[] = [{
    component: Input,
    label: 'referents-full-name',
    required: false,
    message: 'recommended-info',
    name: 'fullName',
},
{
    component: Input,
    label: 'company',
    required: false,
    message: 'recommended-info',
    name: 'company',
},
{
    component: Input,
    label: 'phone-number',
    required: false,
    message: 'recommended-info',
    name: 'phoneNumber',
},
{
    component: Input,
    label: 'email-address',
    required: false,
    message: 'recommended-info',
    name: 'email',
}];
