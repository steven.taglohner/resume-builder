import { memo } from 'react';
import { DropResult } from 'react-beautiful-dnd';
import { useIntl } from 'react-intl';
import { Form } from 'src/components';
import { useSelector, useDispatch } from 'src/hooks/redux';
import { updateResume } from 'src/redux/actions';
import { model } from './model';
import { SECTION } from 'src/types';
import { Reference } from 'src/types/resume';
import { Section, InputSelector } from 'pages/resume/components';
import { reorder } from 'pages/resume/helpers';
import { generateKey } from 'src/utils';

const References = () => {
    const dispatch = useDispatch();
    const { formatMessage } = useIntl();

    const resume = useSelector(state => state.resume.data);

    const handleUpdate = (values: Partial<Reference>, id: string): void => {
        dispatch(
            updateResume({
                updates: resume.references.map(item =>
                    item.id === id ? { ...item, ...values } : item
                ),
                key: SECTION.REFERENCES,
                id: resume.id,
            })
        );
    };

    const handleDragEnd = (result: DropResult) => {
        if (!result.destination) return;

        dispatch(
            updateResume({
                key: SECTION.REFERENCES,
                id: resume.id,
                updates: reorder<Reference>(
                    resume.references,
                    result.source.index,
                    result.destination.index
                )
            })
        );
    };

    const handleCreate = () => {
        dispatch(
            updateResume({
                updates: [...resume.references, { id: generateKey() }],
                key: SECTION.REFERENCES,
                id: resume.id,
                debounce: false,
            })
        );
    };

    const handleDelete = (id: string): void => {
        dispatch(
            updateResume({
                updates: resume.references.filter(reference => reference.id !== id),
                key: SECTION.REFERENCES,
                id: resume.id,
            })
        );
    };

    if (!resume)
        return null

    return (
        <Section>
            <Section.Header
                titleId="references"
            />
            <Section.Skeleton
                data={resume.references}
                onDragEnd={handleDragEnd}
                onDelete={handleDelete}
                fallback={<Section.Fallback
                    title={formatMessage({ id: 'add-reference' })}
                    onClick={handleCreate}
                />}
                header={{
                    getTitle: ({ fullName }) => fullName,
                    getSubtitle: ({ company }) => company
                }}
            >
                {(item: Reference) => (
                    <Form
                        layout="vertical"
                        initialValues={item}
                        onValuesChange={values => handleUpdate(values, item.id)}
                    >
                        <InputSelector model={model} />
                    </Form>
                )}
            </Section.Skeleton>
            {resume.references.length > 0 && <Section.Footer
                buttonTitleId='add-reference'
                onCreate={handleCreate}
            />}
        </Section>
    )
};

export default memo(References);