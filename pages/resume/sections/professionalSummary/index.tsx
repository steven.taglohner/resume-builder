import { memo } from 'react';
import { Section } from 'pages/resume/components';
import { TextEditor } from 'src/components';
import { useSelector, useDispatch } from 'src/hooks/redux';
import { updateResume } from 'src/redux/actions';
import { SECTION } from 'src/types';

const ProfessionalSummary = () => {
    const dispatch = useDispatch();
    const resume = useSelector(state => state.resume.data);

    const handleChange = (description: string) => {
        dispatch(
            updateResume({
                id: resume.id,
                key: SECTION.PROFESSIONAL_SUMMARY,
                updates: { description }
            })
        );
    };

    if (!resume)
        return null

    return (
        <>
            <Section.Header
                titleId='professional-summary'
                descriptionId='professional-summary-description'
            />
            <TextEditor
                value={resume.professionalSummary.description}
                onChange={handleChange}
            />
        </>
    )
};

export default memo(ProfessionalSummary);
