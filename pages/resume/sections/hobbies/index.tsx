import { memo } from 'react';
import { Section } from 'pages/resume/components';
import { TextEditor } from 'src/components';
import { useSelector, useDispatch } from 'src/hooks/redux';
import { updateResume } from 'src/redux/actions';
import { SECTION } from 'src/types';

const Hobbies = () => {
    const dispatch = useDispatch();
    const resume = useSelector(state => state.resume.data);

    const handleChange = (description: string) => {
        dispatch(
            updateResume({
                id: resume.id,
                key: SECTION.HOBBIES,
                updates: { description }
            })
        );
    };

    if (!resume)
        return null

    return (
        <>
            <Section.Header
                titleId='hobbies'
                descriptionId='hobbies-description'
            />
            <TextEditor
                value={resume?.hobbies?.description}
                onChange={handleChange}
            />
        </>
    )
};

export default memo(Hobbies);
