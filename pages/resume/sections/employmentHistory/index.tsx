import { memo } from 'react';
import { useIntl } from 'react-intl';
import { DropResult } from 'react-beautiful-dnd';
import { Form } from 'src/components';
import { useSelector, useDispatch } from 'src/hooks/redux';
import { updateResume } from 'src/redux/actions';
import { model } from './model';
import { SECTION } from 'src/types';
import { EmploymentHistory } from 'src/types/resume';
import { Section, InputSelector } from 'pages/resume/components';
import { getSubtitle, getTitle, reorder } from 'pages/resume/helpers';
import { generateKey } from 'src/utils';

const EmploymentHistory = () => {
    const { formatMessage } = useIntl();
    const dispatch = useDispatch();
    const resume = useSelector(state => state.resume.data);

    const handleUpdate = (values: Partial<EmploymentHistory>, id: string): void => {
        dispatch(
            updateResume({
                updates: resume.employmentHistory.map(item =>
                    item.id === id ? { ...item, ...values } : item
                ),
                key: SECTION.EMPLOYMENT_HISTORY,
                id: resume.id,
            })
        );
    };

    const handleDragEnd = (result: DropResult) => {
        if (!result.destination) return;

        dispatch(
            updateResume({
                key: SECTION.EMPLOYMENT_HISTORY,
                id: resume.id,
                updates: reorder<EmploymentHistory>(
                    resume.employmentHistory,
                    result.source.index,
                    result.destination.index
                )
            })
        );
    };

    const handleCreate = () => {
        dispatch(
            updateResume({
                updates: [...resume.employmentHistory, { id: generateKey() }],
                key: SECTION.EMPLOYMENT_HISTORY,
                id: resume.id,
                debounce: false,
            })
        );
    };

    const handleDelete = (id: string): void => {
        dispatch(
            updateResume({
                updates: resume.employmentHistory.filter(employment => employment.id !== id),
                key: SECTION.EMPLOYMENT_HISTORY,
                id: resume.id,
            })
        );
    };

    if (!resume)
        return null

    return (
        <Section>
            <Section.Header
                titleId="employment-history"
                descriptionId="employment-history-description"
            />
            <Section.Skeleton
                data={resume.employmentHistory}
                onDragEnd={handleDragEnd}
                onDelete={handleDelete}
                fallback={
                    <Section.Fallback
                        title={formatMessage({ id: 'add-employment' })}
                        onClick={handleCreate}
                    />}
                header={{
                    getTitle: ({ jobTitle, employer }) => getTitle({
                        title: jobTitle,
                        employer: employer,
                        preposition: formatMessage({ id: 'at' })
                    }),
                    getSubtitle: ({ range }) => getSubtitle({
                        presentText: formatMessage({ id: 'present' }),
                        range,
                    })
                }}
            >
                {(item: EmploymentHistory) => (
                    <Form
                        layout="vertical"
                        initialValues={item}
                        onValuesChange={values => handleUpdate(values, item.id)}
                    >
                        <InputSelector model={model} />
                    </Form>
                )}
            </Section.Skeleton>
            {resume.employmentHistory.length > 0 && <Section.Footer
                buttonTitleId='add-employment'
                onCreate={handleCreate}
            />}
        </Section>
    )
};

export default memo(EmploymentHistory);