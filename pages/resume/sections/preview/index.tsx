import { useRef, memo, useMemo, useState } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
import { BlobProvider } from '@react-pdf/renderer';
import { shallowEqual } from 'react-redux';
import { PDFDocumentProxy } from 'pdfjs-dist/types/src/display/api';
import { useRect, useSelector } from 'src/hooks';
import {
    Content,
    Spin,
    Header,
    Sider,
    Layout,
    Space,
    Saving,
    Tooltip,
    Pagination,
    Footer
} from 'src/components';
import { Havana } from 'src/templates';
import { useIntl } from 'react-intl';
import classnames from 'classnames';
import {
    AppstoreOutlined,
    DownloadOutlined
} from '@ant-design/icons';
import classes from './preview.module.css';

pdfjs.GlobalWorkerOptions.workerSrc = `//unpkg.com/pdfjs-dist@${pdfjs.version}/build/pdf.worker.min.js`;

const Preview = () => {
    const wrapperRef = useRef<HTMLDivElement>(null);
    const canvasRef = useRef<HTMLCanvasElement>(null);
    const urlRef = useRef<string>('')

    const [collapsed, setCollapsed] = useState(true);
    const [pageNumber, setPageNumber] = useState(1)
    const [totalPages, setTotalPages] = useState(0);

    const windowReact = useRect(wrapperRef.current);

    const loading = useSelector(state => state.resume.update.loading, shallowEqual)
    const resume = useSelector(state => state.resume, (_, newValue) => newValue.update.loading === true)

    const { formatMessage } = useIntl();

    const handleTranslation = (ids: string[]) => ids.reduce(
        (translation, id) => ({
            ...translation, [id]: formatMessage({ id })
        }), {})

    const handleSidebarCollapse = () => {
        setCollapsed(!collapsed)
    }

    const handleDownload = () => {
        urlRef.current
    }

    const handleDocumentLoad = (pdf: PDFDocumentProxy) => {
        setTotalPages(pdf.numPages)
    }

    const handlePageChange = (dir: 1 | -1) => {
        setPageNumber(pageNumber + dir)
    }

    return (
        <Layout>
            <Layout>
                <Header className={classes.header}>
                    <Saving saving={loading} />
                    <Space size={[24, 0]} direction="horizontal">
                        <Tooltip arrowPointAtCenter placement="bottom" title={formatMessage({ id: 'download' })}>
                            <DownloadOutlined
                                className={classes.icon}
                                disabled={!Boolean(urlRef.current)}
                                onClick={handleDownload}
                                download
                            />
                        </Tooltip>
                        <Tooltip arrowPointAtCenter placement="bottomRight" title={formatMessage({ id: 'templates' })}>
                            <AppstoreOutlined
                                className={classes.icon}
                                onClick={handleSidebarCollapse}
                            />
                        </Tooltip>
                    </Space>
                </Header>
                <Footer className={classes.footer} />
                <Content ref={wrapperRef} className={classes.preview}>
                    {resume.data && <BlobProvider
                        document={
                            <Havana
                                getTranslation={handleTranslation}
                                resume={resume.data}
                            />
                        }
                    >
                        {({ url, loading: isLoading }) => isLoading
                            ? <Spin />
                            : <Document
                                file={url}
                                noData={null}
                                loading={null}
                                onLoadSuccess={pdf => handleDocumentLoad(pdf)}
                            >
                                <Page
                                    className={classnames(classes["page-active"], { [classes["page-inactive"]]: loading })}
                                    height={windowReact?.height}
                                    pageNumber={pageNumber}
                                    renderTextLayer={false}
                                    renderAnnotationLayer={false}
                                    loading={null}
                                    noData={null}
                                    renderMode="canvas"
                                    canvasRef={canvasRef}
                                    onRenderSuccess={() => {
                                        urlRef.current = url
                                    }}
                                />
                                {/* <a href={url} className={classes.download}>Download</a> */}
                            </Document>
                        }
                    </BlobProvider>}
                    {useMemo(() => (
                        <Document noData={null} file={urlRef.current} loading={null}>
                            <Page
                                className={classnames(classes["page-active"], { [classes["page-inactive"]]: !loading })}
                                height={windowReact?.height}
                                pageNumber={pageNumber}
                                renderTextLayer={false}
                                renderAnnotationLayer={false}
                                loading={null}
                                noData={null}
                                renderMode="canvas"
                            />
                        </Document>
                    ), [loading, windowReact?.height])}
                </Content>
                <Footer className={classes.footer}>
                    {totalPages > 0 && <Pagination
                        onChange={handlePageChange}
                        current={pageNumber}
                        total={totalPages}
                        className={classes.pagination}
                    />}
                </Footer>
            </Layout>
            <Sider
                theme='light'
                reverseArrow
                trigger={null}
                collapsedWidth={0}
                collapsible
                collapsed={collapsed}
            />
        </Layout>
    );
};

export default memo(Preview);

