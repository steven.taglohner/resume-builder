import { memo } from 'react';
import { useIntl } from 'react-intl';
import { DropResult } from 'react-beautiful-dnd';
import { Form } from 'src/components';
import { useSelector, useDispatch } from 'src/hooks';
import { updateResume } from 'src/redux/actions';
import { model } from './model';
import { SECTION } from 'src/types';
import { Course } from 'src/types/resume';
import { Section, InputSelector } from 'pages/resume/components';
import { getSubtitle, getTitle, reorder } from 'pages/resume/helpers';
import { generateKey } from 'src/utils';

const Courses = () => {
    const { formatMessage } = useIntl();
    const dispatch = useDispatch();

    const resume = useSelector(state => state.resume.data);

    const handleUpdate = (values: Partial<Course>, id: string): void => {
        dispatch(
            updateResume({
                updates: resume.courses.map(item =>
                    item.id === id ? { ...item, ...values } : item
                ),
                key: SECTION.COURSES,
                id: resume.id,
            })
        );
    };

    const handleDragEnd = (result: DropResult) => {
        if (!result.destination) return;

        dispatch(
            updateResume({
                key: SECTION.COURSES,
                id: resume.id,
                updates: reorder<Course>(
                    resume.courses,
                    result.source.index,
                    result.destination.index
                )
            })
        );
    };

    const handleCreate = () => {
        dispatch(
            updateResume({
                updates: [...resume.courses, { id: generateKey() }],
                key: SECTION.COURSES,
                id: resume.id,
                debounce: false
            })
        );
    };

    const handleDelete = (id: string): void => {
        dispatch(
            updateResume({
                updates: resume.courses.filter(employment => employment.id !== id),
                key: SECTION.COURSES,
                id: resume.id,
            })
        );
    };

    if (!resume)
        return null

    return (
        <Section>
            <Section.Header titleId="courses" />
            <Section.Skeleton
                data={resume.courses}
                onDragEnd={handleDragEnd}
                onDelete={handleDelete}
                fallback={<Section.Fallback
                    title={formatMessage({ id: 'add-course' })}
                    onClick={handleCreate}
                />}
                header={{
                    getTitle: ({ course, institution }) => getTitle({
                        title: course,
                        employer: institution,
                        preposition: formatMessage({ id: 'at' })
                    }),
                    getSubtitle: ({ range }) => getSubtitle({
                        presentText: formatMessage({ id: 'present' }),
                        range,
                    })
                }}
            >
                {(item: Course) => (
                    <Form
                        layout="vertical"
                        initialValues={item}
                        onValuesChange={values => handleUpdate(values, item.id)}
                    >
                        <InputSelector model={model} />
                    </Form>
                )}
            </Section.Skeleton>
            {resume.courses.length > 0 && <Section.Footer
                buttonTitleId='add-course'
                onCreate={handleCreate}
            />}
        </Section>
    )
};

export default memo(Courses);