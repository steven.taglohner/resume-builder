import { InputItem } from 'src/types';
import { Input, RangePicker } from 'src/components';

export const model: InputItem[] = [
    {
        component: Input,
        label: 'course',
        required: false,
        message: 'recommended-info',
        name: 'course',
    },
    {
        component: Input,
        label: 'institution',
        required: false,
        message: 'recommended-info',
        name: 'institution',
    },
    {
        component: RangePicker,
        label: 'start-end',
        required: false,
        message: 'recommended-info',
        name: 'range',
    },
    {
        component: Input,
        label: 'city',
        required: false,
        message: 'recommended-info',
        name: 'city',
    },
];
