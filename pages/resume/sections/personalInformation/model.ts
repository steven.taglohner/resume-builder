import { InputItem } from 'src/types';
import { Input, DatePicker } from 'src/components';

const partial: InputItem[] = [{
	component: Input,
	label: 'job-title',
	required: false,
	message: 'recommended-info',
	name: 'jobTitle',
},
{
	component: Input,
	label: 'full-name',
	required: false,
	message: 'recommended-info',
	name: 'fullName',
},
{
	component: Input,
	name: 'email',
	label: 'email-address',
	required: false,
	message: 'recommended-info',
},
{
	component: Input,
	name: 'phoneNumber',
	label: 'phone-number',
	required: false,
	message: 'recommended-info',
},
{
	component: Input,
	name: 'address',
	label: 'address',
	required: false,
	message: 'recommended-info',
},
{
	component: Input,
	name: 'postalCode',
	label: 'postal-code',
	required: false,
	message: 'recommended-info',
},
{
	component: Input,
	name: 'city',
	label: 'city',
	required: false,
	message: 'recommended-info',
},
{
	component: Input,
	name: 'countryName',
	label: 'country-name',
	required: false,
	message: 'recommended-info',
}];

const full: InputItem[] = [...partial, {
	name: 'birthPlace',
	label: 'birth-place',
	component: Input,
	required: false,
	message: 'recommended-info',
},
{
	name: 'nationality',
	label: 'nationality',
	component: Input,
	required: false,
	message: 'recommended-info',
},
{
	name: 'birthDate',
	label: 'birth-date',
	component: DatePicker,
	required: false,
	message: 'recommended-info',
},
{
	name: 'drivingLicense',
	label: 'driving-license',
	component: Input,
	required: false,
	message: 'recommended-info',
},
{
	name: 'website',
	label: 'website',
	component: Input,
	required: false,
	message: 'recommended-info',
}];

export const model = {
	partial,
	full
};