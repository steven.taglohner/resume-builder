import { memo, useState } from 'react';
import { useIntl } from 'react-intl';
import { CaretDownOutlined, CaretUpOutlined } from '@ant-design/icons';
import { Form, Button, Content } from 'src/components';
import { useSelector, useDispatch } from 'src/hooks/redux';
import { Section, InputSelector } from 'pages/resume/components';
import { updateResume } from 'src/redux/actions';
import { Resume, SECTION } from 'src/types';
import { model } from './model';
import classes from './personal-information.module.css'


const PARTIAL_MODEL_SIZE = model.partial.length;

const PersonalInformation = () => {
	const dispatch = useDispatch();
	const { formatMessage } = useIntl();
	const resume = useSelector(state => state.resume.data);

	const [activeModel, setActiveModel] = useState(model.partial);

	const handleUpdate = (_: Partial<Resume>, values: Partial<Resume>) => {
		dispatch(
			updateResume({
				id: resume.id,
				key: SECTION.PERSONAL_INFORMATION,
				updates: values
			})
		);
	};

	const handleActiveModel = () => setActiveModel(
		activeModel.length === PARTIAL_MODEL_SIZE ? model.full : model.partial
	);

	if (!resume)
		return null

	return (
		<Content className={classes.content}>
			<Section.Header titleId='personal-information' />
			<Form
				layout="vertical"
				initialValues={resume.personalInformation}
				onValuesChange={handleUpdate}
			>
				<InputSelector model={activeModel} />
			</Form>
			<Button
				icon={activeModel.length === PARTIAL_MODEL_SIZE
					? <CaretDownOutlined />
					: <CaretUpOutlined />}
				className={classes.button}
				key="drop-down"
				type="link"
				onClick={handleActiveModel}
			>
				{formatMessage({
					id: activeModel.length === PARTIAL_MODEL_SIZE
						? 'show-additional-details'
						: 'hide-additional-details'
				})}
			</Button>
		</Content>
	)
};

export default memo(PersonalInformation);