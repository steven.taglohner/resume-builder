import { memo } from 'react';
import { useIntl } from 'react-intl';
import { DropResult } from 'react-beautiful-dnd';
import { Form } from 'src/components';
import { useSelector, useDispatch } from 'src/hooks/redux';
import { updateResume } from 'src/redux/actions';
import { model } from './model';
import { SECTION } from 'src/types';
import { VolunteerWork } from 'src/types/resume';
import { Section, InputSelector } from 'pages/resume/components';
import { getSubtitle, getTitle, reorder } from 'pages/resume/helpers';
import { generateKey } from 'src/utils';

const VolunteerWork = () => {
    const { formatMessage } = useIntl();
    const dispatch = useDispatch();

    const resume = useSelector(state => state.resume.data);

    const handleUpdate = (values: Partial<VolunteerWork>, id: string): void => {
        dispatch(
            updateResume({
                updates: resume.volunteerWork.map(item =>
                    item.id === id ? { ...item, ...values } : item
                ),
                key: SECTION.VOLUNTEER_WORK,
                id: resume.id,
            })
        );
    };

    const handleDragEnd = (result: DropResult) => {
        if (!result.destination) return;

        dispatch(
            updateResume({
                key: SECTION.VOLUNTEER_WORK,
                id: resume.id,
                updates: reorder<VolunteerWork>(
                    resume.volunteerWork,
                    result.source.index,
                    result.destination.index
                )
            })
        );
    };

    const handleCreate = () => {
        dispatch(
            updateResume({
                updates: [...resume.volunteerWork, { id: generateKey() }],
                key: SECTION.VOLUNTEER_WORK,
                id: resume.id,
                debounce: false,
            })
        );
    };

    const handleDelete = (id: string): void => {
        dispatch(
            updateResume({
                updates: resume.volunteerWork.filter(employment => employment.id !== id),
                key: SECTION.VOLUNTEER_WORK,
                id: resume.id,
            })
        );
    };

    // TODO: decide later how to handle empty data
    if (!resume)
        return null

    return (
        <Section>
            <Section.Header titleId="volunteer-work" />
            <Section.Skeleton
                data={resume.volunteerWork}
                onDragEnd={handleDragEnd}
                onDelete={handleDelete}
                fallback={
                    <Section.Fallback
                        title={formatMessage({ id: 'add-volunteer-work' })}
                        onClick={handleCreate}
                    />}
                header={{
                    getTitle: ({ title, employer }) => getTitle({
                        title,
                        employer: employer,
                        preposition: formatMessage({ id: 'at' })
                    }),
                    getSubtitle: ({ range }) => getSubtitle({
                        presentText: formatMessage({ id: 'present' }),
                        range,
                    })
                }}
            >
                {(item: VolunteerWork) => (
                    <Form
                        layout="vertical"
                        initialValues={item}
                        onValuesChange={values => handleUpdate(values, item.id)}
                    >
                        <InputSelector model={model} />
                    </Form>
                )}
            </Section.Skeleton>
            {resume.volunteerWork.length > 0 && <Section.Footer
                buttonTitleId='add-volunteer-work'
                onCreate={handleCreate}
            />}
        </Section>
    )
};

export default memo(VolunteerWork);