import { memo } from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'src/components';
import { useSelector, useDispatch } from 'src/hooks/redux';
import { updateResume } from 'src/redux/actions';
import { model } from './model';
import { SECTION } from 'src/types';
import { Education } from 'src/types/resume';
import { Section, InputSelector } from 'pages/resume/components';
import { getSubtitle, getTitle, reorder } from 'pages/resume/helpers';
import { generateKey } from 'src/utils';

const Education = () => {
    const { formatMessage } = useIntl();
    const dispatch = useDispatch();
    const resume = useSelector(state => state.resume.data);

    const handleUpdate = (values: Partial<Education>, id: string): void => {
        dispatch(
            updateResume({
                updates: resume.education.map(item =>
                    item.id === id ? { ...item, ...values } : item
                ),
                key: SECTION.EDUCATION,
                id: resume.id,
            })
        );
    };

    const handleDragEnd = (result: any) => {
        if (!result.destination) return;

        dispatch(
            updateResume({
                key: SECTION.EDUCATION,
                id: resume.id,
                updates: reorder<Education>(
                    resume.education,
                    result.source.index,
                    result.destination.index
                )
            })
        );
    };

    const handleCreate = () => {
        dispatch(
            updateResume({
                updates: [...resume.education, { id: generateKey() }],
                key: SECTION.EDUCATION,
                id: resume.id,
                debounce: false
            })
        );
    };

    const handleDelete = (id: string): void => {
        dispatch(
            updateResume({
                updates: resume.education.filter(employment => employment.id !== id),
                key: SECTION.EDUCATION,
                id: resume.id,
            })
        );
    };

    if (!resume)
        return null

    return (
        <Section>
            <Section.Header
                titleId="education"
                descriptionId="education-description"
            />
            <Section.Skeleton
                data={resume.education}
                onDragEnd={handleDragEnd}
                onDelete={handleDelete}
                fallback={<Section.Fallback
                    title={formatMessage({ id: 'add-education' })}
                    onClick={handleCreate}
                />}
                header={{
                    getTitle: ({ degree, school }) => getTitle({
                        title: degree,
                        employer: school,
                        preposition: formatMessage({ id: 'at' })
                    }),
                    getSubtitle: ({ range }) => getSubtitle({
                        presentText: formatMessage({ id: 'present' }),
                        range
                    })
                }}
            >
                {(item: Education) => (
                    <Form
                        layout="vertical"
                        initialValues={item}
                        onValuesChange={values => handleUpdate(values, item.id)}
                    >
                        <InputSelector model={model} />
                    </Form>
                )}
            </Section.Skeleton>
            {resume.education.length > 0 && <Section.Footer
                buttonTitleId='add-education'
                onCreate={handleCreate}
            />}
        </Section>
    )
};

export default memo(Education);