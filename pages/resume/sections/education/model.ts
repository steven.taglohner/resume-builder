import { InputItem } from 'src/types';
import { Input, RangePicker, TextEditor } from 'src/components';

export const model: InputItem[] = [{
    component: Input,
    label: 'degree',
    required: false,
    message: 'recommended-info',
    name: 'degree',
},
{
    component: Input,
    label: 'school',
    required: false,
    message: 'recommended-info',
    name: 'school',
},
{
    component: RangePicker,
    label: 'start-end',
    required: false,
    message: 'recommended-info',
    name: 'range',
},
{
    component: Input,
    label: 'city',
    required: false,
    message: 'recommended-info',
    name: 'city',
},
{
    component: TextEditor,
    label: 'description',
    required: false,
    message: 'recommended-info',
    name: 'description',
    columnProps: {
        lg: 24
    }
}];
