import { Content } from 'src/components';
import { PublicLayout } from 'src/layouts';

const Home = () => {

  return (
    <PublicLayout>
      <Content></Content>
    </PublicLayout>
  );
};

export default Home;
