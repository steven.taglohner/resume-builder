import Document, {
	DocumentProps,
	Html,
	Head,
	Main,
	NextScript,
} from 'next/document';

export default class MyDocument extends Document<DocumentProps> {
	render() {
		return (
			<Html>
				<Head>
					<link
						href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap"
						rel="stylesheet"
					/>
					<link
						href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap"
						rel="stylesheet"
					/>
					<link
						href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap"
						rel="stylesheet"
					/>
					<meta charSet="utf-8" />
				</Head>
				<body>
					<Main />
					<NextScript />
					<style>
						{`#__next {
							height: 100vh;
							width: 100vw;
							display: flex;
						}`}
					</style>
				</body>
			</Html>
		);
	}
}