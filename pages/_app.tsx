import { ConnectedRouter } from 'connected-next-router';
import { UserProvider } from '@auth0/nextjs-auth0';
import { IntlProvider } from 'react-intl';
import { wrapper } from 'src/redux/store';
import english from 'src/lang/en-US.json';
import portugues from 'src/lang/pt-BR.json';

import 'antd/dist/antd.css';
import 'src/styles/global.css'

const App = ({ Component, pageProps }) => (
  <IntlProvider messages={english} locale="en">
    <UserProvider user={pageProps.user}>
      <ConnectedRouter>
        <Component {...pageProps} />
      </ConnectedRouter>
    </UserProvider>
  </IntlProvider>
);

export default wrapper.withRedux(App);
