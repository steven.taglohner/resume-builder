import { getSession } from '@auth0/nextjs-auth0';
import { Error } from 'mongoose';
import { NextApiResponse } from 'next';
import { withApiAuth } from 'src/HOCs';
import { Resume } from 'src/mongoose/models';
import { connectDatabase } from 'src/services/mongodb/database';
import { Resume as Document, OPERATION } from 'src/types';
import { getUserfromSession, handleApiResponse } from 'src/utils';


export default withApiAuth(
    (request, response: NextApiResponse<Document | Error>) => new Promise<void>(
        async () => {
            try {
                await connectDatabase();

                const session = getSession(request, response);
                const body = request.body;

                if (!session.user.sub)
                    throw new Error('Missing session information.');

                const { uid } = getUserfromSession(session);

                switch (request.query.operation) {
                    case OPERATION.CREATE:
                        Resume.create(
                            { uid },
                            (error, document) => handleApiResponse(response, error, document)
                        );

                        break;
                    case OPERATION.GET:
                        Resume.findById(
                            body.id,
                            (error: Error, document: Document) => handleApiResponse(
                                response,
                                error,
                                document,
                            )
                        );

                        break;
                    case OPERATION.DELETE:
                        Resume.findByIdAndDelete(body.id,
                            (error: Error, document: Document) => handleApiResponse(
                                response,
                                error,
                                document,
                                false
                            )
                        );

                    case OPERATION.UPDATE:
                        const { id, key, updates } = body;

                        Resume.findByIdAndUpdate(id, { [key]: updates }, { upsert: true, new: true },
                            (error: Error, document: Document) => handleApiResponse(
                                response,
                                error,
                                document,
                            )
                        );

                        break;
                    default:
                        throw new Error('Unknown operation.');
                }

            } catch (error) {
                throw new Error(`Failed to initialize database.Error: ${error}`);
            }
        }
    )
);