import { withApiAuthRequired, getSession } from '@auth0/nextjs-auth0';
import { Error } from 'mongoose';
import { NextApiResponse } from 'next';
import { Resume } from 'src/mongoose/models';
import { connectDatabase } from 'src/services/mongodb/database';
import { Resume as Document } from 'src/types';
import { getUserfromSession, handleApiResponse } from 'src/utils';

export default (request, response: NextApiResponse<Document | Error>) => new Promise<void>(
    async () => {
        try {
            await connectDatabase();

            const session = getSession(request, response);

            if (!session.user.sub)
                throw new Error('Missing session information.');

            const { uid } = getUserfromSession(session);

            Resume.find({ uid }, (error: Error, document: Document) => {
                handleApiResponse(response, error, document)
            })
        } catch (error) {
            throw new Error(`Failed to initialize database.Error: ${error}`);
        }
    }
)


