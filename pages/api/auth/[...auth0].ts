import { handleAuth, handleLogin } from '@auth0/nextjs-auth0';
import { ROUTE } from 'src/types';

export default handleAuth({
    async login(req, res) {
        await handleLogin(req, res, {
            returnTo: ROUTE.DASHBOARD,
        });
    },
});