
import { createReducer } from '@reduxjs/toolkit';
import {
    GET_RESUME_LIST_PENDING,
    GET_RESUME_LIST_FULFILLED,
    GET_RESUME_LIST_REJECTED,
    CREATE_RESUME_FULFILLED,
    CREATE_RESUME_PENDING,
    CREATE_RESUME_REJECTED,
    DELETE_RESUME_FULFILLED,
    GET_RESUME_PENDING,
    GET_RESUME_FULFILLED,
    GET_RESUME_REJECTED,
    DELETE_RESUME_PENDING,
    DELETE_RESUME_REJECTED,
    UPDATE_RESUME_FULFILLED,
    UPDATE_RESUME_PENDING,
    UPDATE_RESUME_REJECTED,
    ResumeState
} from 'src/redux/types';

const initialState: ResumeState = {
    list: [],
    data: undefined,
    get: {
        loading: false,
        error: null,
    },
    create: {
        loading: false,
        error: null,
    },
    delete: {
        loading: false,
        error: null,
    },
    update: {
        loading: false,
        error: null,
    },
};

export const resume = createReducer(initialState, {
    [GET_RESUME_LIST_PENDING]: state => {
        state.get = { loading: true, error: null };
    },
    [GET_RESUME_LIST_FULFILLED]: (state, { payload: { data } }) => {
        state.get = initialState.get;
        state.list = data;
    },
    [GET_RESUME_LIST_REJECTED]: (state, { payload }) => {
        state.get = { loading: false, error: payload.message };
    },
    [GET_RESUME_PENDING]: state => {
        state.get = { loading: true, error: null };
    },
    [GET_RESUME_FULFILLED]: (state, { payload: { data } }) => {
        state.get = initialState.get;
        state.data = data;
    },
    [GET_RESUME_REJECTED]: (state, { payload }) => {
        state.get = { loading: false, error: payload.message };
    },
    [CREATE_RESUME_PENDING]: state => {
        state = initialState;
        state.create = { loading: true, error: null };
    },
    [CREATE_RESUME_FULFILLED]: (state, { payload: { data } }) => {
        state.create = initialState.get;
        state.data = data;
    },
    [CREATE_RESUME_REJECTED]: (state, { payload }) => {
        state.create = { loading: false, error: payload.message };
    },
    [DELETE_RESUME_PENDING]: (state, { payload }) => {
        state.delete = { loading: true, error: null };
        state.list = state.list.filter(resume => resume.id !== payload.id);
    },
    [DELETE_RESUME_FULFILLED]: state => {
        state.delete = initialState.delete;
    },
    [DELETE_RESUME_REJECTED]: (state, { payload }) => {
        state.delete = { loading: false, error: payload.message };
    },
    [UPDATE_RESUME_PENDING]: (state, { payload }) => {
        const { updates, key } = payload;

        state.update = { loading: true, error: null };
        state.data = { ...state.data, [key]: updates };
    },
    [UPDATE_RESUME_FULFILLED]: state => {
        state.update = initialState.update;
    },
    [UPDATE_RESUME_REJECTED]: (state, { payload }) => {
        const { message } = payload;

        state.update = { loading: false, error: message };
    },
});