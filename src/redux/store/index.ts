import { createRouterMiddleware, initialRouterState, routerReducer as router, RouterState } from 'connected-next-router';
import { createWrapper, HYDRATE } from 'next-redux-wrapper';
import { AnyAction, combineReducers, CombinedState } from 'redux';
import { configureStore } from '@reduxjs/toolkit';
import promise from 'redux-promise-middleware';
import { ResumeState } from 'src/redux/types';
import { resume } from 'src/redux/reducers';
import Router from 'next/router';

export const rootReducer = combineReducers({
    resume,
    router,
});

const reducer = (state: CombinedState<{ resume: ResumeState, router: RouterState }>, action: AnyAction) => {
    if (action.type === HYDRATE) {
        const nextState = {
            ...state,
            ...action.payload,
        };

        if (typeof window !== 'undefined' && state.router)
            nextState.router = state.router;

        return nextState;
    } else {
        return rootReducer(state, action);
    }
};

export const makeStore = context => {
    const routerMiddleware = createRouterMiddleware();
    const { asPath } = context.ctx || Router.router || {};

    let preloadedState: {
        router: RouterState;
    };

    if (asPath)
        preloadedState = {
            router: initialRouterState(asPath)
        };

    return configureStore({
        reducer,
        preloadedState,
        middleware: config => config({
            serializableCheck: false
        }).concat(promise, routerMiddleware)
    });
};

export const wrapper = createWrapper(makeStore);
