import { Resume } from 'src/types';

export type ResumeState = {
    list: Partial<Resume>[],
    data?: Resume,
    create: {
        loading: boolean
        error?: TypeError
    },
    delete: {
        loading: boolean
        error?: TypeError
    },
    get: {
        loading: boolean
        error?: TypeError
    },
    update: {
        loading: boolean
        error?: TypeError
    }
}
