import { rootReducer, makeStore } from 'src/redux/store';
import { Action, ThunkAction } from '@reduxjs/toolkit';

export type AppDispatch = ReturnType<typeof makeStore>['dispatch']
export type RootState = ReturnType<typeof rootReducer>
export type Thunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>