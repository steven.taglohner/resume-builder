import { AxiosResponse } from 'axios';
import {
    GET_RESUME,
    DELETE_RESUME,
    CREATE_RESUME,
    GET_RESUME_LIST,
    UPDATE_RESUME
} from 'src/redux/types/constants';
import { Resume } from 'src/types';

type GetResumeListAction = {
    type: typeof GET_RESUME_LIST
    payload: Promise<AxiosResponse>
}

type GetResumeAction = {
    type: typeof GET_RESUME
    payload: Promise<AxiosResponse>
}

type DeleteResumeAction = {
    type: typeof DELETE_RESUME
    payload: {
        data: Partial<Resume>
        promise: Promise<AxiosResponse>
    }
}

type CreateResumeAction = {
    type: typeof CREATE_RESUME
    payload: Promise<AxiosResponse>
}

type UpdateResumeAction = {
    type: typeof UPDATE_RESUME
    payload: {
        data: {
            updates: Partial<Resume>
            key: keyof Resume
        }
        promise: Promise<AxiosResponse>
    }
}

export type UpdateResume<T> = {
    id: string,
    key: keyof Resume,
    updates: T,
    debounce?: boolean
}

export type ResumeActionTypes =
    | GetResumeListAction
    | GetResumeAction
    | CreateResumeAction
    | UpdateResumeAction
    | DeleteResumeAction

