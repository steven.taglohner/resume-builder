import { Dispatch } from 'redux';
import { ResumeActionTypes, Thunk, UpdateResume } from 'src/redux/types';
import {
    GET_RESUME_LIST, GET_RESUME, CREATE_RESUME, DELETE_RESUME, UPDATE_RESUME
} from 'src/redux/types';
import { push } from 'connected-next-router';
import { Resume, ROUTE } from 'src/types';
import { axios, promiseTimeout } from 'src/utils';
import { URL } from 'src/routes';

export const getResumeList = (): ResumeActionTypes => ({
    type: GET_RESUME_LIST,
    payload: axios.post<Resume>(URL.GET_RESUME_LIST)
});

export const getResume = (id: string): ResumeActionTypes => ({
    type: GET_RESUME,
    payload: axios.post<Resume>(URL.GET_RESUME, { id })
});

export const createResume = () => (dispatch: Dispatch): ResumeActionTypes => ({
    type: CREATE_RESUME,
    payload: axios.get<Resume>(URL.CREATE_RESUME).then(response => {
        dispatch(
            push(ROUTE.RESUME + response.data.id)
        );

        return response;
    }),
});

export const deleteResume = (id: string): ResumeActionTypes => ({
    type: DELETE_RESUME,
    payload: {
        data: { id },
        promise: axios.patch<Resume>(URL.DELETE_RESUME, { id })
    },
});

var container = {};

export const updateResume = <T>({ id, key, updates, debounce = true }: UpdateResume<T>): Thunk =>
    (dispatch: Dispatch) => {
        container = { ...container, [key]: updates };

        dispatch({
            type: UPDATE_RESUME,
            payload: {
                data: { updates, key },
                promise: promiseTimeout(key, () => axios.patch<Resume>(
                    URL.UPDATE_RESUME, { key, updates, id }
                ), debounce ? 2000 : 0)
            }
        });
    };