import mongoose from 'mongoose';

const MONGODB_URI = process.env.MONGODB_URI;

if (!MONGODB_URI)
    throw new Error('Missing MONGODB_URI environment variable.');

/**
 * Global is used here to maintain a cached connection across hot reloads
 * in development. This prevents connection growing exponentially  during API Route usage.
 */
let cached = global.mongoose;

if (!cached)
    cached = global.mongoose = { connection: null, promise: null };

export const connectDatabase = async () => {
    if (cached.connection)
        return cached.connection;

    if (!cached.promise)
        cached.promise = await mongoose.connect(MONGODB_URI, { bufferCommands: false });

    cached.connection = await cached.promise;

    return cached.connection;
};
