import { createContext } from "react";
import { Resume } from "src/types";

export const ResumeContext = createContext<Resume | undefined>(undefined);