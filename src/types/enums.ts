export enum SECTION {
    PERSONAL_INFORMATION = 'personalInformation',
    PROFESSIONAL_SUMMARY = 'professionalSummary',
    EMPLOYMENT_HISTORY = 'employmentHistory',
    EDUCATION = 'education',
    COURSES = 'courses',
    SKILLS = 'skills',
    INTERNSHIPS = 'internships',
    LANGUAGES = 'languages',
    REFERENCES = 'references',
    VOLUNTEER_WORK = 'volunteerWork',
    HOBBIES = 'hobbies',
}

export enum OPERATION {
    CREATE = 'create',
    GET = 'get',
    UPDATE = 'update',
    DELETE = 'delete',
    UNKNOWN = 'unknown'
}

export enum ROUTE {
    HOME = '/',
    RESUME = '/resume/',
    DASHBOARD = '/dashboard',
}

export enum INPUT_COMPONENT {
    TEXT_FIELD = 'textField',
    DATE_PICKER = 'datePicker',
    RANGE_PICKER = 'rangePicker',
    CHECKBOX = 'checkbox',
    TEXT_EDITOR = 'textEditor',
    LEVEL_SELECTOR = 'levelSelector'
}

export enum LEVEL {
    BEGINNER = 'beginner',
    INTERMEDIATE = 'intermediate',
    ADVANCED = 'advanced'
}