export type ValueOf<T> = T[keyof T];

export type MapOf<T> = {
    [K in keyof T]: ValueOf<T>
}