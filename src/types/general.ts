import { Moment } from 'moment';
import { FormItemProps } from 'antd';
import { CheckboxChangeEvent as AntCheckboxChangeEvent } from 'antd/lib/checkbox';
import { SegmentedValue as AntSegmentedValue } from 'antd/lib/segmented';
import { SECTION } from "src/types"
import { ColProps } from 'src/components';


export type InputItem = FormItemProps & {
    label: string
    required: boolean
    message: string
    component: any,
    columnProps?: ColProps
}

export type CheckboxChangeEvent = AntCheckboxChangeEvent

export type RangePickerValue = {
    start?: Moment,
    end?: Moment,
    present?: boolean
}

export type SegmentedValue = AntSegmentedValue

export type SectionItem = {
    section: SECTION
    name: string
    icon: any
}