import { Moment } from 'moment';

export type Resume = {
    id: string,
    uid?: string
    title?: string
    createdAt: string
    updatedAt: string
    settings: Settings
    personalInformation: PersonalInformation
    professionalSummary: ProfessionalSummary
    employmentHistory?: EmploymentHistory[]
    education?: Education[]
    courses?: Course[]
    internships?: Internship[]
    volunteerWork?: VolunteerWork[]
    skills: Skills
    languages: Languages
    references?: Reference[]
    hobbies: Hobbies
}

export type Settings = {
    template: string
    color: string
    locale: string
}

export type PersonalInformation = {
    jobTitle?: string
    fullName?: string
    email?: string
    avatarUrl?: string
    phoneNumber?: number
    nationality?: string
    countryName?: string
    city?: string
    address?: string
    postalCode?: string
    birthPlace?: string
    drivingLicense?: string
    website?: string
    birthDate?: Date
}

export type ProfessionalSummary = {
    description?: string
}

export type EmploymentHistory = {
    id: string
    jobTitle?: string
    employer?: string
    city?: string
    country?: string
    description?: string
    range?: { start: Moment, end: Moment, present: boolean }
    present?: boolean
}

export type Education = {
    id: string
    degree?: string
    school?: string
    city?: string
    description?: string
    range?: { start: Moment, end: Moment, present: boolean }
    present?: boolean
}

export type Course = {
    id: string
    course: string
    institution: string
    description: string
    range?: { start: Moment, end: Moment, present: boolean }
    present: boolean
}

export type Internship = {
    id: string
    title: string
    employer: string
    city: string
    country: string
    description: string
    range?: { start: Moment, end: Moment, present: boolean }
    present: boolean
};

export type VolunteerWork = {
    id: string
    title: string
    employer: string
    city: string
    description: string
    range?: { start: Moment, end: Moment, present: boolean }
    present: boolean
}

export type Skill = {
    id: string
    skill: string
    level: string
}

export type Skills = {
    data: Skill[],
    config: {
        hideLevel: boolean
    }
}

export type Language = {
    id: string
    language: string
    level: string
}

export type Languages = {
    data: Language[],
    config: {
        hideLevel: boolean
    }
}

export type Reference = {
    id: string
    fullName: string
    jobTitle: string
    company: string
    city: string
    phoneNumber: number
    email: string
}

export type Hobbies = {
    description?: string
}

export type ResumeSection =
    Settings |
    PersonalInformation |
    ProfessionalSummary |
    EmploymentHistory |
    Education |
    Course |
    Internship |
    VolunteerWork |
    Skill |
    Language |
    Reference |
    Hobbies