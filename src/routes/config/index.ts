import { ROUTE } from 'src/types';

export type Config = {
    redirectPath: string
    privateRoute: boolean
}

export type RouteConfig = {
    [key: string]: Config
}

export const config: RouteConfig = {
    dashboard: {
        redirectPath: ROUTE.HOME,
        privateRoute: true,
    },
    home: {
        redirectPath: ROUTE.DASHBOARD,
        privateRoute: false,
    },
    resume: {
        redirectPath: ROUTE.DASHBOARD,
        privateRoute: true,
    },
};