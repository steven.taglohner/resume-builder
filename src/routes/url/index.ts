export const baseUrl = 'http://localhost:8080';

export const URL = {
    CREATE_RESUME: '/api/resume/create',
    GET_RESUME: '/api/resume/get',
    DELETE_RESUME: '/api/resume/delete',
    UPDATE_RESUME: '/api/resume/update',
    GET_RESUME_LIST: '/api/resume/list',
    LOGOUT: '/api/auth/logout',
    LOGIN: '/api/auth/login'
};


