import { ComponentType } from 'react';
import { useRouter } from 'next/router';
import { NextPage, NextPageContext } from 'next';
import { isBrowser } from 'src/utils';

type Props<T> = {
	(props: T): {
		redirectPath: string,
		shouldRedirect: boolean
	}
}

export const withRedirect = <T extends object>(callback: Props<T>) => <P extends object>(Component: ComponentType<P>) => {
	const WithRedirectWrapper: NextPage & ComponentType<P & T> = props => {
		const router = useRouter();
		const { redirectPath, shouldRedirect } = callback(props);

		if (isBrowser && shouldRedirect) {
			router.push(redirectPath);

			return null;
		}

		return <Component {...props} />;
	};

	WithRedirectWrapper.getInitialProps = async (context: NextPageContext) => {
		// TODO: implement server side redirect
		return {};
	};

	return WithRedirectWrapper;
};
