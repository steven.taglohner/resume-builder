import { useState, useEffect, ComponentType } from 'react';
import { Spin } from 'src/components';
import styles from './loader.module.css';

type Props<T> = {
	(props: T): {
		loading?: boolean
		delay?: number
	}
}

export const withLoading = <T extends object>(callback: Props<T>) => <P extends object>(
	Component: ComponentType<P>
) => {
	const WithLoadingWrapper: ComponentType<P & T> = props => {
		const [finished, setFinished] = useState(false);

		useEffect(() => {
			const { loading, delay = 1000 } = callback(props);

			if (!loading) {
				setTimeout(
					() => setFinished(true), delay
				);
			}
		}, [props]);

		return finished
			? <Component {...props} />
			: <div className={styles.container}><Spin /></div>;
	};

	return WithLoadingWrapper;
};