import { Page, View, Document } from '@react-pdf/renderer';
import { Resume } from 'src/types';
import {
    Intro,
    Profile,
    Details,
    Employment,
    Education,
    Internships,
    Skills
} from './sections';
import { styles } from './styles'

type HavanaProps = {
    resume: Resume,
    getTranslation: (ids: string[]) => { [key: string]: string }
}

export const Havana = ({ resume, getTranslation }: HavanaProps) => {
    const translation = getTranslation([
        'details',
        'profile',
        'employment-history',
        'education',
        'internships',
        'present',
        'skills',
        'at',
    ])

    if (!resume)
        return null

    const {
        personalInformation,
        professionalSummary,
        employmentHistory,
        education,
        internships,
        skills
    } = resume

    return (
        <Document>
            <Page wrap size="LETTER" style={styles.page}>
                <View style={styles.header}>
                    <Intro data={personalInformation} />
                </View>
                <View style={styles.layout}>
                    <View style={styles.main}>
                        <Profile
                            translation={translation}
                            data={professionalSummary}
                        />
                        <Employment
                            translation={translation}
                            data={employmentHistory}
                        />
                        <Education
                            translation={translation}
                            data={education}
                        />
                        <Internships
                            translation={translation}
                            data={internships}
                        />
                    </View>
                    <View style={styles.sider}>
                        <Details
                            data={personalInformation}
                            translation={translation}
                        />
                        <Skills
                            data={skills}
                            translation={translation}
                        />
                    </View>
                </View>
            </Page>
        </Document>
    );
}