import { memo } from 'react'
import { Text, View } from '@react-pdf/renderer';
import { PersonalInformation } from 'src/types/resume';
import { styles } from '../../styles'

type IntroProps = {
    data?: PersonalInformation
}

const Intro = ({ data }: IntroProps) => {
    if (!data)
        return null

    return (
        <View>
            <Text style={styles.h2}>
                {data?.fullName}
            </Text>
            <Text style={styles.secondary}>
                {data?.jobTitle}
            </Text>
        </View>
    )
}

export default memo(Intro)