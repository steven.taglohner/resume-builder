import { memo } from 'react'
import { Text, View } from '@react-pdf/renderer';
import Html from 'react-pdf-html';
import { ProfessionalSummary } from 'src/types/resume';
import { convertDraftToHtml, hasDraftText } from 'src/utils';
import { styles, styleSheet } from '../../styles'

type ProfileProps = {
    data?: ProfessionalSummary,
    translation: { [key: string]: string }
}

const Profile = ({ data, translation }: ProfileProps) => {
    const hasText = hasDraftText(data?.description)

    if (!data)
        return null

    if (!hasText)
        return null

    return (
        <View style={styles.section}>
            <Text style={styles.h4}>
                {translation.profile}
            </Text>
            <View style={styles.item}>
                {hasDraftText(data.description) && <Html stylesheet={styleSheet}>
                    {convertDraftToHtml(data.description)}
                </Html>}
            </View>
        </View>
    )
}

export default memo(Profile)