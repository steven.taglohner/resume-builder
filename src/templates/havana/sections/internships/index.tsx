import { memo } from 'react'
import { Text, View } from '@react-pdf/renderer';
import Html from 'react-pdf-html';
import { Internship } from 'src/types/resume';
import { convertDraftToHtml, hasDraftText } from 'src/utils';
import { styles, styleSheet } from '../../styles'
import { getSubtitle, getTitle } from 'src/templates/helpers';

type InternshipsProps = {
    data: Internship[],
    translation: { [key: string]: string }
}

const Internships = ({ data, translation }: InternshipsProps) => {
    if (data.length === 0)
        return null

    return (
        <View style={styles.section}>
            <Text style={styles.h4}>
                {translation.internships}
            </Text>
            {data.map(
                (internship, index) => (
                    <View
                        key={internship.id + index}
                        style={styles.item}
                    >
                        <View style={styles.itemHeader}>
                            <Text style={styles.h5}>
                                {getTitle({
                                    title: internship.title,
                                    employer: internship.employer,
                                    preposition: translation.at
                                })}
                            </Text>
                            <Text style={styles.secondary}>
                                {getSubtitle({
                                    range: internship.range,
                                    presentText: translation.present
                                })}
                            </Text>
                        </View>
                        <View>
                            {hasDraftText(internship.description) && <Html stylesheet={styleSheet}>
                                {convertDraftToHtml(internship.description)}
                            </Html>}
                        </View>
                    </View>
                ))
            }
        </View>
    )
}

export default memo(Internships);