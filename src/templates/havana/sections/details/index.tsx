import { memo } from 'react'
import { Text, View, Link } from '@react-pdf/renderer';
import { PersonalInformation } from 'src/types/resume';
import { styles } from '../../styles'
import { validateObject } from 'src/utils';

type DetailsProps = {
    data: PersonalInformation,
    translation: { [key: string]: string }
}

const Details = ({ data, translation }: DetailsProps) => {
    const isValidObject = validateObject(
        data, ['address', 'phoneNumber', 'email'], true
    )

    if (!isValidObject)
        return null

    return (
        <View style={styles.section}>
            <Text style={styles.h4}>
                {translation.details}
            </Text>
            <View style={styles.item}>
                <Text style={styles.body}>
                    {data?.address}
                </Text>
                <Text style={styles.body}>
                    {data?.phoneNumber}
                </Text>
                <Link src={data?.email} style={styles.link}>
                    {data?.email}
                </Link>
            </View>
        </View>
    )
}

export default memo(Details)