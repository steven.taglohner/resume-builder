import { memo } from 'react'
import { Text, View } from '@react-pdf/renderer';
import { Skills } from 'src/types/resume';
import { styles } from '../../styles'

type SkillsProps = {
    data: Skills,
    translation: { [key: string]: string }
}

const Skills = ({ data: skills, translation }: SkillsProps) => {
    if (skills.data.length === 0)
        return null

    return (
        <View style={styles.section}>
            <Text style={styles.h4}>
                {translation.skills}
            </Text>
            {skills.data.map(
                (item, index) => (
                    <View key={item?.skill + index} style={styles.item}>
                        <Text style={styles.body}>
                            {item?.skill}
                        </Text>
                        {!skills.config.hideLevel && item.skill && <Text style={styles.secondary}>
                            {item.level}
                        </Text>}
                    </View>
                ))
            }
        </View>
    )
}

export default memo(Skills)