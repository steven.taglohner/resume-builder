import { memo } from 'react'
import { Text, View } from '@react-pdf/renderer';
import Html from 'react-pdf-html';
import { EmploymentHistory } from 'src/types/resume';
import { convertDraftToHtml, hasDraftText } from 'src/utils';
import { getSubtitle, getTitle } from 'src/templates/helpers';
import { styles, styleSheet } from '../../styles'

type EmploymentProps = {
    data: EmploymentHistory[],
    translation: { [key: string]: string }
}

const Employment = ({ data, translation }: EmploymentProps) => {
    if (data.length === 0)
        return null

    return (
        <View style={styles.section}>
            <Text style={styles.h4}>
                {translation['employment-history']}
            </Text>
            {data.map(
                (employment, index) => (
                    <View
                        key={employment.id + index}
                        style={styles.item}
                    >
                        <View style={styles.itemHeader}>
                            <Text style={styles.h5}>
                                {getTitle({
                                    title: employment.jobTitle,
                                    employer: employment.employer,
                                    preposition: translation.at
                                })}
                            </Text>
                            <Text style={styles.secondary}>
                                {getSubtitle({
                                    range: employment.range,
                                    presentText: translation.present
                                })}
                            </Text>
                        </View>
                        <View>
                            {hasDraftText(employment.description) && <Html stylesheet={styleSheet}>
                                {convertDraftToHtml(employment.description)}
                            </Html>}
                        </View>
                    </View>
                )
            )}
        </View>
    )
}

export default memo(Employment);