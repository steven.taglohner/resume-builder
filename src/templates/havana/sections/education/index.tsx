import { memo } from 'react'
import { Text, View } from '@react-pdf/renderer';
import Html from 'react-pdf-html';
import { Education } from 'src/types/resume';
import { convertDraftToHtml, hasDraftText } from 'src/utils';
import { getSubtitle, getTitle } from 'src/templates/helpers';
import { styles, styleSheet } from '../../styles'

type EducationProps = {
    data: Education[],
    translation: { [key: string]: string }
}

const Education = ({ data, translation }: EducationProps) => {
    if (data.length === 0)
        return null

    return (
        <View style={styles.section}>
            <Text style={styles.h4}>
                {translation.education}
            </Text>
            <View>
                {data.map(
                    (education, index) =>
                        <View key={education.id + index} style={styles.item}>
                            <Text style={styles.h5}>
                                {getTitle({
                                    title: education.degree,
                                    preposition: translation.at,
                                    employer: education.school,
                                })}
                            </Text>
                            <Text style={styles.secondary}>
                                {getSubtitle({
                                    range: education.range,
                                    presentText: translation.present
                                })}
                            </Text>
                            <View style={styles.content}>
                                {hasDraftText(education.description) && <Html stylesheet={styleSheet}>
                                    {convertDraftToHtml(education.description)}
                                </Html>}
                            </View>
                        </View>
                )}
            </View>
        </View>
    )
}

export default memo(Education);