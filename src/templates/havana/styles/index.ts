import { StyleSheet, Font } from '@react-pdf/renderer';
import SourceSansPro from 'src/templates/fonts/source-sans-pro.ttf'
import SourceSansProBold from 'src/templates/fonts/source-sans-pro-bold.ttf'
import SourceSansProSemiBold from 'src/templates/fonts/source-sans-pro-semi-bold.ttf'

Font.register({
    family: 'source-sans-pro', fonts: [
        { src: SourceSansPro },
        { src: SourceSansProSemiBold, fontWeight: 500 },
        { src: SourceSansProBold, fontWeight: 600 },
    ]
})

export const styles = StyleSheet.create({
    page: {
        padding: 40,
        paddingTop: 30
    },
    layout: {
        flexDirection: 'row',
    },
    header: {

    },
    main: {
        flex: 1,
    },
    sider: {
        marginLeft: 30,
    },
    h1: {
        fontFamily: 'source-sans-pro',
        fontSize: 28.86,
        fontWeight: 500,
        lineHeight: 1.4,
    },
    h2: {
        fontFamily: 'source-sans-pro',
        fontSize: 20.74,
        fontWeight: 500,
        lineHeight: 1.4,
    },
    h3: {
        fontFamily: 'source-sans-pro',
        fontSize: 17.28,
        fontWeight: 500,
        lineHeight: 1.4,
    },
    h4: {
        fontFamily: 'source-sans-pro',
        fontSize: 14.4,
        fontWeight: 500,
        lineHeight: 1.4,
    },
    h5: {
        fontFamily: 'source-sans-pro',
        fontSize: 12,
        fontWeight: 500,
        lineHeight: 1.4,
    },
    body: {
        fontFamily: 'source-sans-pro',
        fontSize: 10,
        fontWeight: 400,
        lineHeight: 1.4,
    },
    title: {
        fontFamily: 'source-sans-pro',
        fontSize: 10,
        fontWeight: 500,
        lineHeight: 1.4,
    },
    secondary: {
        fontFamily: 'source-sans-pro',
        fontSize: 10,
        fontWeight: 400,
        lineHeight: 1.4,
        color: 'rgb(0, 0, 0, .6)'
    },
    link: {
        fontFamily: 'source-sans-pro',
        fontSize: 10,
        fontWeight: 400,
        lineHeight: 1.4,
        color: '#1A91F0',
    },
    section: {
        marginTop: 10,
    },
    item: {
        marginTop: 4,
    },
    itemHeader: {
        marginBottom: 2
    },
    content: {
        marginTop: 4
    },
});

export const styleSheet = {
    p: {
        ...styles.body,
        padding: 0,
        margin: 0,
    },
}
