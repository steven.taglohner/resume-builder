import moment from 'moment';
import { RangePickerValue } from 'src/types';

type GetTitleParams = {
    title?: string,
    employer?: string,
    preposition?: string
}

export const getTitle = ({ title, employer, preposition }: GetTitleParams) => {
    if (title && employer && preposition)
        return `${title} ${preposition} ${employer}`;

    return title;
};

type GetSubtitle = {
    range?: RangePickerValue,
    format?: string,
    presentText: string
}

export const getSubtitle = ({ range, presentText, format = 'MMM YYYY', }: GetSubtitle) => {
    let result: string | undefined;

    if (range?.start && range?.end) {
        const start = moment(range.start).format(format);
        const end = moment(range.end).format(format);

        result = `${start} - ${end}`;
    }

    if (range?.start && range?.present) {
        const start = moment(range.start).format(format);

        result = `${start} - ${presentText}`;
    }

    return result;
};
