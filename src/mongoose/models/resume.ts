import { model, Document } from 'mongoose';
import { Resume as ResumeSchema } from 'src/mongoose/schemas';
import { Resume as ResumeType } from 'src/types/resume';

export const Resume = model<ResumeType & Document>('Resume', ResumeSchema, 'resumes');