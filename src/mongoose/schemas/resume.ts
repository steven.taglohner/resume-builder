import { Schema, models } from 'mongoose';
import * as Type from 'src/types/resume';

for (const key in models) {
    delete models[key];
}

const options = {
    virtuals: true,
};

const key = 'toJSON';

export const EmploymentHistory = new Schema<Type.EmploymentHistory>({
    id: { type: String, default: undefined },
    jobTitle: { type: String, default: undefined },
    employer: { type: String, default: undefined },
    city: { type: String, default: undefined },
    country: { type: String, default: undefined },
    description: { type: String, default: undefined },
    range: { type: { start: Date, end: Date, present: Boolean }, default: undefined },
    present: { type: Boolean, default: undefined },
});

export const Education = new Schema<Type.Education>({
    id: { type: String, default: undefined },
    degree: { type: String, default: undefined },
    school: { type: String, default: undefined },
    city: { type: String, default: undefined },
    description: { type: String, default: undefined },
    range: { type: { start: Date, end: Date, present: Boolean }, default: undefined },
    present: { type: Boolean, default: undefined },
});

export const Course = new Schema<Type.Course>({
    id: { type: String, default: undefined },
    course: { type: String, default: undefined },
    institution: { type: String, default: undefined },
    description: { type: String, default: undefined },
    range: { type: { start: Date, end: Date, present: Boolean }, default: undefined },
    present: { type: Boolean, default: undefined },
});

export const Internship = new Schema<Type.Internship>({
    id: { type: String, default: undefined },
    title: { type: String, default: undefined },
    employer: { type: String, default: undefined },
    city: { type: String, default: undefined },
    country: { type: String, default: undefined },
    description: { type: String, default: undefined },
    range: { type: { start: Date, end: Date, present: Boolean }, default: undefined },
});

export const VolunteerWork = new Schema<Type.VolunteerWork>({
    id: { type: String, default: undefined },
    title: { type: String, default: undefined },
    employer: { type: String, default: undefined },
    city: { type: String, default: undefined },
    description: { type: String, default: undefined },
    range: { type: { start: Date, end: Date, present: Boolean }, default: undefined },
    present: { type: Boolean, default: undefined },
});

export const Skill = new Schema<Type.Skill>({
    id: { type: String, default: undefined },
    skill: { type: String, default: undefined },
    level: { type: String, default: undefined },
});

export const Language = new Schema<Type.Language>({
    id: { type: String, default: undefined },
    language: { type: String, default: undefined },
    level: { type: String, default: undefined },
});

export const Reference = new Schema<Type.Reference>({
    id: { type: String, default: undefined },
    fullName: { type: String, default: undefined },
    jobTitle: { type: String, default: undefined },
    company: { type: String, default: undefined },
    city: { type: String, default: undefined },
    phoneNumber: { type: Number, default: undefined },
    email: { type: String, default: undefined },
});

export const Resume = new Schema<Type.Resume>({
    createdAt: { type: String, default: undefined },
    updatedAt: { type: String, default: undefined },
    uid: { type: String, default: undefined },
    title: { type: String, default: 'Untitled' },
    settings: {
        template: { type: String, default: undefined },
        color: { type: String, default: undefined },
        locale: { type: String, default: 'en' },
    },
    personalInformation: {
        jobTitle: { type: String, default: undefined },
        fullName: { type: String, default: undefined },
        email: { type: String, default: undefined },
        phoneNumber: { type: Number, default: undefined },
        nationality: { type: String, default: undefined },
        countryName: { type: String, default: undefined },
        city: { type: String, default: undefined },
        address: { type: String, default: undefined },
        avatarUrl: { type: String, default: undefined },
        postalCode: { type: String, default: undefined },
        birthPlace: { type: String, default: undefined },
        birthDate: { type: Date, default: undefined },
        drivingLicense: { type: String, default: undefined },
        website: { type: String, default: undefined },
    },
    professionalSummary: {
        description: { type: String, default: undefined },
    },
    employmentHistory: [EmploymentHistory],
    education: [Education],
    courses: [Course],
    internships: [Internship],
    volunteerWork: [VolunteerWork],
    skills: {
        data: [Skill],
        config: {
            hideLevel: false
        }
    },
    languages: {
        data: [Language],
        config: {
            hideLevel: false
        }
    },
    references: [Reference],
    hobbies: {
        description: { type: String, default: undefined },
    }
}, { timestamps: true, minimize: false }).set(key, options);
