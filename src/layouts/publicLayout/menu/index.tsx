import { Fragment } from 'react';
import { useIntl } from 'react-intl';
import Link from 'next/link';
import { URL } from 'src/routes/url';
import styles from './menu.module.css';


export const Menu = () => {
	const { formatMessage } = useIntl();

	return (
		<Fragment>
			<Link href={URL.LOGIN}>
				<a className={styles.login}>{formatMessage({ id: 'login' })}</a>
			</Link>
		</Fragment>
	);
};
