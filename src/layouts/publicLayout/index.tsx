import { ReactElement } from 'react';
import { Layout, Header } from 'src/components';
import { Menu } from './menu';
import styles from './public.module.css';

type Props = {
	children: ReactElement
}

const PublicLayout = ({ children }: Props) => {
	return (
		<Layout>
			<Header className={styles.header}>
				<Menu />
			</Header>
			<Layout className={styles.layout}>
				{children}
			</Layout>
		</Layout>
	);
};

export default PublicLayout;
