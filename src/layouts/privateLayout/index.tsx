import { Layout, Header } from 'src/components';
import { Menu } from './menu';
import styles from './private.module.css';

const PrivateLayout = ({ children }) => (
	<Layout>
		<Header className={styles.container}>
			<Menu />
		</Header>
		<Layout className={styles.layout}>{children}</Layout>
	</Layout>
);

export default PrivateLayout;
