import { Fragment } from 'react';
import { useUser } from '@auth0/nextjs-auth0';
import { useIntl } from 'react-intl';
import { URL } from 'src/routes/url';
import { Avatar } from 'src/components';
import styles from './menu.module.css';


export const Menu = () => {
	const { formatMessage } = useIntl();
	const { user } = useUser();

	return (
		<Fragment>
			<a href={URL.LOGOUT} className={styles.logout}>{formatMessage({ id: 'logout' })}</a>
			<Avatar src={user.picture} />
		</Fragment>
	);
};
