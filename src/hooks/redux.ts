import {
    TypedUseSelectorHook, useDispatch as useAppDispatch, useSelector as useAppSelector
} from 'react-redux';
import type { AppDispatch, RootState } from 'src/redux/types';

export const useDispatch = () => useAppDispatch<AppDispatch>();
export const useSelector: TypedUseSelectorHook<RootState> = useAppSelector;