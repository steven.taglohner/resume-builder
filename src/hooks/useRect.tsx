import { useCallback, useLayoutEffect, useState } from 'react';
import ResizeObserver from 'resize-observer-polyfill';

type RectResult = {
    bottom: number;
    height: number;
    left: number;
    right: number;
    top: number;
    width: number;
} | null;

const getRect = (element: Element | null): RectResult | null => {
    if (!element) return null;
    return element.getBoundingClientRect();
};

export const useRect = (element: HTMLElement): RectResult => {
    const [rect, setRect] = useState(getRect(element));

    const handleResize = useCallback(() => {
        if (!element) return;

        setRect(getRect(element));
    }, [element]);

    useLayoutEffect(() => {
        if (!element) return;

        handleResize();
        if (typeof ResizeObserver === 'function') {
            let resizeObserver: ResizeObserver | null = new ResizeObserver(() =>
                handleResize()
            );
            resizeObserver.observe(element);

            return () => {
                if (!resizeObserver) return;
                resizeObserver.disconnect();
                resizeObserver = null;
            };
        }

        window.addEventListener('resize', handleResize);

        return () => window.removeEventListener('resize', handleResize);
    }, [element, handleResize]);

    return rect;
};