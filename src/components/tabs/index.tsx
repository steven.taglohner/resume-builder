import { Tabs as AntTabs, TabsProps as AntTabsProps } from 'antd';

export type TabsProps = AntTabsProps & {
	children: any
}

export const Tabs = ({ children, ...rest }: TabsProps) => (
	<AntTabs {...rest}>{children}</AntTabs>
);

Tabs.TabPane = AntTabs.TabPane;