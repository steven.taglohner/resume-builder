import { Layout as AntLayout, LayoutProps as AntLayoutProps } from 'antd';

export type LayoutProps = AntLayoutProps & {}

export const Layout = (props: LayoutProps) => <AntLayout {...props} />;

export const Content = AntLayout.Content;
export const Footer = AntLayout.Footer;
export const Header = AntLayout.Header;
export const Sider = AntLayout.Sider;