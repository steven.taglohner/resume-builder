import { Segmented, Space } from 'antd';
import { useIntl } from 'react-intl';
import { lowerCase } from 'lodash';
import { LEVEL } from 'src/types';
import styles from './level-selector.module.css';

type LevelSelector = {
	value: string
	onChange?: (value: string) => void
	model?: string[]
	disabled?: boolean
}

export const LevelSelector = ({ value: level, onChange, model, disabled }: LevelSelector) => {
	const { formatMessage } = useIntl();

	const handleChange = (value: string) => {
		if (onChange)
			onChange(value);
	};

	return (
		<Space direction='vertical' className={styles.space}>
			<Segmented
				block
				disabled={disabled}
				value={level}
				onChange={handleChange}
				options={model.map(
					level => formatMessage({ id: lowerCase(level) })
				)}
			/>
		</Space>
	);
};

LevelSelector.defaultProps = {
	model: Object.values(LEVEL)
}