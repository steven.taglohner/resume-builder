import { Space as AntSpace, SpaceProps as AntSpaceProps } from 'antd';

export type SpaceProps = AntSpaceProps & {}

export const Space = (props: SpaceProps) => <AntSpace {...props} />;
