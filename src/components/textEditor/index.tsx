import { useState, useRef, Fragment } from 'react';
import { Editor, EditorState, convertFromRaw, convertToRaw } from 'draft-js';
import { useDidMount } from 'src/hooks';
// import { ToolBar } from './components';
import styles from './text-editor.module.css';
import 'draft-js/dist/Draft.css';

export type TextEditorProps = {
    onChange: (rawState: string) => void
    value?: string
}

export const TextEditor = ({ onChange, value }: TextEditorProps) => {
    const editorRef = useRef<Editor>(null);

    const [editorState, setEditorState] = useState(
        () => EditorState.createEmpty()
    );

    useDidMount(() => {
        if (editorRef.current) {
            editorRef.current.editorContainer.className = styles.container;
            editorRef.current.editor.className = styles.editor;
        }
    });

    useDidMount(() => {
        if (value) {
            const parsedContent = JSON.parse(value);
            const initialContent = convertFromRaw(parsedContent);
            const newContent = EditorState.createWithContent(initialContent);

            setEditorState(newContent);
        }
    });

    const handleChange = (editorState: EditorState) => {
        setEditorState(editorState);

        const currentContent = editorState.getCurrentContent();
        const rawState = convertToRaw(currentContent);
        const stringfiedState = JSON.stringify(rawState);

        onChange(stringfiedState);
    };

    return (
        <Fragment>
            {/* <ToolBar /> */}
            <Editor
                ref={editorRef}
                editorState={editorState}
                onChange={handleChange}
                stripPastedStyles
            />
        </Fragment>
    );
};
