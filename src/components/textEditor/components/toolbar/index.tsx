import { Segmented, Button, Typography } from 'antd';
import { useIntl } from 'react-intl';
import {
    CaretDownFilled,
    AlignLeftOutlined,
    AlignCenterOutlined,
    AlignRightOutlined,
    LinkOutlined,
    OrderedListOutlined,
    UnorderedListOutlined,
    ClearOutlined,
    BoldOutlined
} from '@ant-design/icons';
import { Space, Select } from 'src/components';
import styles from './toolbar.module.css';

export const ToolBar = () => {
    const { formatMessage } = useIntl();

    return (
        <div className={styles.toolbar}>
            <Space>
                <Select suffixIcon={<CaretDownFilled />} defaultValue="paragraph">
                    <Select.Option value="paragraph">Paragraph</Select.Option>
                    <Select.Option value="heading 1">Heading 1</Select.Option>
                    <Select.Option value="heading 2">Heading 2</Select.Option>
                    <Select.Option value="heading 3">Heading 3</Select.Option>
                </Select>
                <Space size="middle" align="center" direction="horizontal">
                    <Button.Group>
                        <Button className={styles.bold}>Bold</Button >
                        <Button className={styles.underline}>Underlined</Button>
                        <Button className={styles.italic}>Italic</Button>
                    </Button.Group>
                </Space>
                <Segmented
                    options={[
                        {
                            value: 'alignLeft',
                            icon: <AlignLeftOutlined />,
                        },
                        {
                            value: 'alignCenter',
                            icon: <AlignCenterOutlined />,
                        },
                        {
                            value: 'alignRight',
                            icon: <AlignRightOutlined />,
                        },
                    ]}
                />
                <Button.Group>
                    <Button>UL</Button>
                    <Button>OL</Button>
                </Button.Group>
                <Button icon={<LinkOutlined />}></Button>
                <Button icon={<ClearOutlined />}></Button>
            </Space>





        </div >
    );
};