import { Collapse as AntCollapse, CollapseProps } from 'antd';
import { Panel as CustomPanel } from './panel';
import styles from './collapse.module.css';

export const Collapse = (props: CollapseProps) => (
	<AntCollapse className={styles.collapse} expandIconPosition="end" {...props} />
);

export const Panel = CustomPanel;
