import { Text, Content } from 'src/components';
import styles from './header.module.css';

type HeaderProps = {
    title?: string,
    subtitle?: string
    placeholder?: string
}

export const Header = ({ title, subtitle, placeholder }: HeaderProps) => (
    <Content className={styles.header}>
        {title
            ? <Text strong>{title}</Text>
            : <Text type='danger' strong>{placeholder}</Text>}
        {title && subtitle && <Text>{subtitle}</Text>}
    </Content>
);

Header.defaultProps = {
    placeholder: 'Not Specified'
};