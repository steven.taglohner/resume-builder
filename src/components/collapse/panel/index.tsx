import { ReactNode } from 'react';
import { Collapse, CollapsePanelProps as AntCollapsePanelProps } from 'antd';

export type CollapsePanelProps = {
	children: ReactNode | ReactNode[]
	footer?: ReactNode
} & AntCollapsePanelProps

export const Panel = ({ children, footer, ...rest }: CollapsePanelProps) => (
	<Collapse.Panel  {...rest}>
		{children}
		{footer}
	</Collapse.Panel>
);

Panel.defaultProps = {
	showArrow: true,
};