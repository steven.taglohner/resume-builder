import { Spin as AntSpin, SpinProps } from 'antd';

export type LoadingIndicatorProps = SpinProps & {}

export const Spin = (props: LoadingIndicatorProps) => (
	<AntSpin {...props} />
);
