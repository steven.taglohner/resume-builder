import { Checkbox as AntCheckbox, CheckboxProps as AntCheckboxProps } from 'antd';

export type CheckboxProps = AntCheckboxProps & {}

export const Checkbox = (props: CheckboxProps) => <AntCheckbox {...props} />;
