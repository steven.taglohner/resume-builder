import { PageHeader as AntPageheader, PageHeaderProps as AntPageHeaderProps } from 'antd';

export type PageHeaderProps = AntPageHeaderProps & {}

export const PageHeader = (props: PageHeaderProps) => (
	<AntPageheader {...props} />
);
