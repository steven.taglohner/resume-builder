import { Button as AntButton, ButtonProps as AntButtonProps } from 'antd';

export type ButtonProps = AntButtonProps & {}

export const Button = ({ children, ...rest }: ButtonProps) => (
	<AntButton {...rest}>{children}</AntButton>
);

Button.Group = AntButton.Group;