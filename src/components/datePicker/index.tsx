import { DatePicker as AntDatePicker, DatePickerProps as AntDatePickerProps } from 'antd';
import moment, { Moment } from 'moment';
import { RangePicker as CustomerRangePicker } from './rangePicker';
import styles from './date-picker.module.css';

export type DatePickerProps = AntDatePickerProps & {}

const DATE_FORMAT = 'MM/DD/YYYY';

export const DatePicker = ({ defaultValue, value, ...rest }: DatePickerProps) => {
	const formatValue = (value: Moment) => value ? moment(value).format(DATE_FORMAT) : '';

	return (
		<AntDatePicker
			format={formatValue}
			defaultValue={moment(defaultValue)}
			value={value}
			className={styles.picker}
			allowClear
			{...rest}
		/>
	);
};

export const RangePicker = CustomerRangePicker;
