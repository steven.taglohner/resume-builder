import { DatePicker, TimeRangePickerProps, Checkbox, Typography, Space } from 'antd';
import { useIntl } from 'react-intl';
import moment, { Moment } from 'moment';
import { RangePickerValue, CheckboxChangeEvent } from 'src/types';
import styles from './range-picker.module.css';

const DATE_FORMAT = 'MM/YYYY';

export type RangePickerProps = Omit<TimeRangePickerProps, 'value' | 'onChange'> & {
	onChange?: (value: RangePickerValue) => void;
	value?: RangePickerValue,
}

export const RangePicker = ({ format, value, onChange, ...rest }: RangePickerProps) => {
	const { formatMessage } = useIntl();

	const handleCheck = (e: CheckboxChangeEvent) => onChange({
		start: value?.start,
		end: e.target.checked ? moment() : value?.end,
		present: e.target.checked
	});

	const handleChange = (values: [Moment, Moment]) => onChange({
		start: values?.[0], end: values?.[1], present: value?.present
	});

	return (
		<Space size="small" direction="vertical" className={styles.container}>
			<DatePicker.RangePicker
				format={DATE_FORMAT}
				disabled={[false, value?.present]}
				allowEmpty={[false, value?.present]}
				value={[moment(value?.start), moment(value?.end)]}
				onChange={handleChange}
				className={styles.picker}
				allowClear={false}
				picker="month"
				{...rest}
			/>
			<Space>
				<Checkbox onChange={handleCheck} checked={value?.present} />
				<Typography.Text>
					{formatMessage({ id: 'present' })}
				</Typography.Text>
			</Space>
		</Space>
	);
};
