import { Dropdown as AntDropdown, DropdownProps as AntDropdownProps } from 'antd';

export type DropdownProps = AntDropdownProps & {}

export const Dropdown = ({ children, ...rest }: DropdownProps) => (
	<AntDropdown {...rest}>{children}</AntDropdown>
);

Dropdown.Button = AntDropdown.Button;