import {
    useEffect, useRef, useState
} from 'react';
import lottie, { AnimationItem } from 'lottie-web';
import classnames from 'classnames';
import classes from './save-animation.module.css';

interface SaveAnimation {
    visible: boolean,
    className?: string,
}

export const SaveAnimation = ({ visible, className }) => {
    const containerRef = useRef<HTMLDivElement>(null);
    const [animation, setAnimation] = useState<AnimationItem | undefined>(undefined);

    useEffect(() => {
        if (containerRef.current) {
            const lottieAnimation = lottie.loadAnimation({
                container: containerRef.current,
                animationData: require('src/assets/animations/cloud-uploading.json'),
                renderer: 'svg',
                loop: true,
                autoplay: true,
            })

            setAnimation(lottieAnimation)
        }

        () => {
            if (animation)
                animation.destroy()
        }
    }, [visible])


    return (
        <div
            ref={containerRef}
            className={classnames(classes.animation, { [classes['animation-visible']]: visible })}
        />
    )
}

SaveAnimation.defaultProps = {
    className: ''
}