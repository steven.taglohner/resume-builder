import { Divider as AntDivider, DividerProps as AntDividerProps } from 'antd';

export type DividerProps = AntDividerProps & {}

export const Divider = ({ children, ...rest }: DividerProps) => (
	<AntDivider {...rest}>{children}</AntDivider>
);