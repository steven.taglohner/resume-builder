import { Select as AntSelect, SelectProps as AntSelectProps } from 'antd';

export type SelectProps = AntSelectProps & {}

export const Select = ({ children, ...rest }: SelectProps) => (
	<AntSelect {...rest}>{children}</AntSelect>
);

Select.Option = AntSelect.Option;