import { ReactNode } from 'react';
import {
	DragDropContext,
	DragDropContextProps,
	Droppable as ReactDroppable,
	DroppableProps as ReactDroppableProps,
} from 'react-beautiful-dnd';

export type DroppableProps = {
	children: ReactNode
} & DragDropContextProps &
	Omit<ReactDroppableProps, 'children'>

export const Droppable = ({ children, onDragEnd, droppableId }: DroppableProps) => (
	<DragDropContext onDragEnd={onDragEnd}>
		<ReactDroppable droppableId={droppableId}>
			{provided => (
				<div {...provided.droppableProps} ref={provided.innerRef}>
					{children}
					{provided.placeholder}
				</div>
			)}
		</ReactDroppable>
	</DragDropContext>
);

Droppable.defaultProps = {
	droppableId: 'droppable',
};
