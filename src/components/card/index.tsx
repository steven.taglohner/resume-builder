import { Card as AntCard, CardProps as AntCardProps } from 'antd';
import styles from './card.module.css'

export type CardProps = AntCardProps & {}

export const Card = (props: CardProps) => (
	<AntCard className={styles.body} {...props} />
);

Card.Meta = AntCard.Meta;
