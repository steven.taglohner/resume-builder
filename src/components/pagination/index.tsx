import { Button, Space, Typography } from 'antd'
import { RightOutlined, LeftOutlined } from '@ant-design/icons';

export type PaginationPros = {
    current: number,
    total: number,
    onChange: (dir: 1 | -1) => void,
    className?: string,
}

export const Pagination = ({ current, total, onChange, className }: PaginationPros) => {
    const handleChange = (dir: 1 | -1) => {
        onChange(dir)
    }

    return (
        <Space className={className}>
            <Button
                disabled={current === 1}
                shape="circle"
                icon={<LeftOutlined />}
                onClick={() => handleChange(-1)}
                size="small"
                type="primary"
            />
            <Typography.Text>
                {current + "/" + total}
            </Typography.Text>
            <Button
                disabled={current === total}
                shape="circle"
                icon={<RightOutlined />}
                onClick={() => handleChange(1)}
                size="small"
                type="primary"
            />
        </Space>
    )
}