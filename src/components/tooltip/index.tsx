import { Tooltip as AntTooltip, TooltipProps as AntTooltipProps } from 'antd';

export type TooltipProps = AntTooltipProps & {}

export const Tooltip = ({ children, ...rest }: TooltipProps) => (
	<AntTooltip {...rest}>{children}</AntTooltip>
);
