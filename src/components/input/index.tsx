import { Input as AntInput, InputProps as AntInputProps } from 'antd';
import classes from './input.module.css'

export type InputProps = AntInputProps & {}

export const Input = (props: InputProps) => <AntInput className={classes.input} {...props} />;
