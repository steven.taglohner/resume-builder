import { Menu as AntMenu, MenuProps as AntMenuProps } from 'antd';

export type MenuProps = AntMenuProps & {}

export const Menu = ({ children, ...rest }: MenuProps) => (
	<AntMenu {...rest}>{children}</AntMenu>
);