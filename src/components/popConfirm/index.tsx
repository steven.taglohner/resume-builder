import React from 'react'
import { Popconfirm as AntPopConfirm, PopconfirmProps as AntPopConfirmProps } from 'antd'

export type PopConfirmProps = AntPopConfirmProps & {
	children: any
}

export const PopConfirm = ({ children, ...rest }: PopConfirmProps) => (
	<AntPopConfirm {...rest}>{children}</AntPopConfirm>
)
