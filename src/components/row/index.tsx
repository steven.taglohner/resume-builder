import { Row as AntRow, RowProps as AntRowProps } from 'antd';
import classes from './row.module.css';

export type RowProps = AntRowProps & {}

export const Row = (props: RowProps) => <AntRow className={classes.row} {...props} />;
