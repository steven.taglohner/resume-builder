import { Avatar as AntAvatar, AvatarProps as AntAvatarProps } from 'antd';

export type AvatarProps = AntAvatarProps & {}

export const Avatar = (props: AvatarProps) => (
	<AntAvatar {...props} />
);
