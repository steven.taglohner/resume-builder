import { Typography } from 'antd';
import { TextProps } from 'antd/lib/typography/Text';

export type TextPropss = TextProps

export const Text = (props: TextPropss) => <Typography.Text copyable={false}  {...props} />;
