import { Typography as AntTypography } from 'antd';

export const Typography = AntTypography;
export { Text } from './text';
export const Link = Typography.Link;
export const Paragraph = Typography.Paragraph;
export const Title = Typography.Title;
