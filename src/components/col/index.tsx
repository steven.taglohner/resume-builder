import { Col as AntCol, ColProps as AntColProps } from 'antd';

export type ColProps = AntColProps & {}

export const Col = (props: ColProps) => <AntCol {...props} />;
