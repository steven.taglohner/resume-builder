import { Radio as AntRadio, RadioProps as AntRadioProps } from 'antd';

export type RadioProps = AntRadioProps & {}

export const Radio = ({ children, ...rest }: RadioProps) => (
	<AntRadio {...rest}>{children}</AntRadio>
);

Radio.Group = AntRadio.Group;
Radio.Button = AntRadio.Button;