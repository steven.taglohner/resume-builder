import { ReactNode } from 'react';
import {
	Draggable as ReactDraggable,
	DraggableProps as ReactDraggableProps,
} from 'react-beautiful-dnd';
import { MoreOutlined } from '@ant-design/icons';
import { cloneChild } from 'src/utils';
import styles from './styles.module.scss';

export type DraggableProps = {
	children: ReactNode | ReactNode[]
} & Omit<ReactDraggableProps, 'children'>

export const Draggable = ({ draggableId, index, children }: DraggableProps) => (
	<ReactDraggable draggableId={draggableId} index={index}>
		{(provided, snapshot) => (
			<div
				ref={provided.innerRef}
				{...provided.draggableProps}
				className={styles['draggable__container']}
				style={{ ...provided.draggableProps.style }}
			>
				<div className={styles['draggable__grip']} {...provided.dragHandleProps}>
					<MoreOutlined />
				</div>
				{cloneChild(children, { style: { opacity: snapshot.isDragging ? 0.5 : 1 } })}
			</div>
		)}
	</ReactDraggable>
);
