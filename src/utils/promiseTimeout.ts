var timeouts = {};

export const promiseTimeout = <T>(key: string, callback: () => Promise<T>, ms = 2000) => new Promise(
    (resolve, reject) => {
        clearTimeout(
            timeouts[key]
        );

        timeouts[key] = setTimeout(async () => {
            try {
                const response = await callback();
                resolve(response);
            } catch (error) {
                reject(error);
            }
        }, ms);
    }
);
