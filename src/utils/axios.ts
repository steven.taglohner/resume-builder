import axios from 'axios';

axios.create({
    withCredentials: true,
});

// axios.interceptors.request.use(async config => {

// 	const token = await auth.currentUser?.getIdToken()

// 	if (token && config.headers) {
// 		config.headers.token = token
// 		config.headers['Access-Control-Allow-Origin'] = "*"
// 	}

// 	return config
// }, error => {
// 	return Promise.reject(error)
// })

export default axios;
