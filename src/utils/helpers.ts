import { ReactNode, cloneElement, Children, isValidElement } from 'react';
import { Session } from '@auth0/nextjs-auth0';

export const getUserfromSession = (session: Session) => {
    const sub = session.user.sub;
    const pipe = sub.indexOf('|') + 1;
    const uid = sub.substr(pipe);

    return { ...session, uid };
};

export const generateKey = () => {
    return 'xxxxxxxx-xxxx-xxxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (Math.random() * 16) | 0,
            v = c == 'x' ? r : (r & 0x3) | 0x8;
        return v.toString(16);
    });
};

export const concatWithNumber = (key: string, number: number) => key.concat(
    number.toString()
);

export const isBrowser = () => typeof window !== 'undefined';


export const cloneChildWithName = <T>(children: ReactNode | ReactNode[], name: string, data?: T) =>
    Children.map(children, child => {
        if (!isValidElement(child))
            return;

        if (typeof child.type === 'string')
            return;

        if (child.type.name === name)
            return cloneElement(child, data);

        return null;
    });

export const cloneChild = <T>(children: ReactNode | ReactNode[], data: T) =>
    Children.map(children, child => {
        if (!isValidElement(child))
            return;

        if (typeof child.type === 'string')
            return;

        return cloneElement(child, data);
    });

export const validateObject = (obj: Object, keys: string[], partial: boolean = false): boolean => {
    return partial
        ? keys.some(
            key => obj.hasOwnProperty(key) && Boolean(obj[key])
        )

        : keys.every(
            key => obj.hasOwnProperty(key) && Boolean(obj[key])
        )
}
