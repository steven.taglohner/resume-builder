import { NextApiResponse } from 'next';

type ExtendedDocument = {
    uid?: string
}

export const handleApiResponse = <T extends ExtendedDocument>(
    response: NextApiResponse,
    error: Error,
    document?: T,
    send = true,
) => {
    if (document) {
        if (send) {
            response.status(200).send(document)
        } else {
            response.status(200)
        }
    }

    if (error) {
        response.status(422).send(
            error ?? new Error('Document not found.')
        )
    }
};