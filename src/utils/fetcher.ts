
export const fetcher = async (url: string) => {
    try {
        const data = await fetch(url);

        return data.json();
    } catch (error) {
        throw Error(error);
    }
};