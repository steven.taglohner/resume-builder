import draftToHtml from 'draftjs-to-html';
import { convertFromRaw } from 'draft-js'

export const convertDraftToHtml = (value: string) => {
    const rawContent = JSON.parse(value);

    return draftToHtml(rawContent)
}

export const hasDraftText = (value?: string): boolean => {
    if (!value)
        return false

    const rawContent = JSON.parse(value);
    const hasText = convertFromRaw(rawContent).hasText()

    return hasText
}